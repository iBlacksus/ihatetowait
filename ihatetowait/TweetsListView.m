//
//  TweetsListController.m
//  ihatetowait
//
//  Created by iBlacksus on 01.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TweetsListView.h"
#import "SpacialMasterController.h"
#import "ASDepthModalViewController.h"
#import "AppDelegate.h"
#import "Tweets.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TimeView.h"
#import <AudioToolbox/AudioToolbox.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import <SDWebImage/UIImageView+WebCache.h>

#define udShowingTweetsListHands @"showingTweetsListHands"

#define bankRatingRate1 10
#define bankRatingRate2 20
#define bankRatingRate3 30
#define bankRatingRate4 40

#define bankBestTimeText @"Лучшее время для посещения:"
#define bankBadTimeText @"Худшее время для посещения:"

@interface TweetsListView ()
{
    AppDelegate *appDelegate;
    NSFetchedResultsController *fetchedResultsController;
    BOOL dontShowLoadingView;
    BOOL dontShowSplash;
}

@end

@implementation TweetsListView

@synthesize bankName;
@synthesize bankAccount;
@synthesize bankCoordinate;
@synthesize bankLatLongRange;
@synthesize showHeaderView;
@synthesize delegate;

- (void)initView
{
    self.frame = [SpacialMasterController childFrame];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    dontShowLoadingView = NO;
    mainNavigationItem.title = bankName;
    dontShowSplash = [[NSUserDefaults standardUserDefaults] boolForKey:udShowingTweetsListHands];
    
    if (!showHeaderView) {
        mainTableView.tableHeaderView = nil;
        [mainTableView reloadData];
    }
    else {
        headerBankName.text = bankName;
        headerBankAccount.text = bankAccount;
        headerBankStreet.text = [NSString stringWithFormat:@"%f, %f", bankCoordinate.latitude, bankCoordinate.longitude];
        [self reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:bankCoordinate.latitude longitude:bankCoordinate.longitude]];
    }
}

- (IBAction)close:(id)sender
{
    [delegate TweetsListViewDone];
}

- (void)reverseGeocodeLocation:(CLLocation *)location
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    if (reverseGeocoder) {
        [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (!error && placemarks.count > 0) {
                CLPlacemark* placemark = [placemarks objectAtIndex:0];
                NSMutableString *place = [NSMutableString string];
                if (placemark.locality) {
                    [place appendString:placemark.locality];
                }
                if (placemark.thoroughfare) {
                    if (place.length > 0) {
                        [place appendString:@", "];
                    }
                    [place appendString:placemark.thoroughfare];
                }
                if (placemark.subThoroughfare) {
                    if (place.length > 0) {
                        [place appendString:@", "];
                    }
                    [place appendString:placemark.subThoroughfare];
                }
                headerBankStreet.text = place;
            }
        }];
    }
}

- (void)dataLoadingDone
{
    loadingView.hidden = YES;
    dontShowLoadingView = YES;
    fetchedResultsController = nil;
    [mainTableView reloadData];
    
    if (showHeaderView) {
        [self loadBankAvatar];
        [self setRating];
        [self setBestAndBadTime];
    }
    
    if (!dontShowSplash) {
        if (fetchedResultsController.fetchedObjects.count > 0) {
            dontShowSplash = YES;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udShowingTweetsListHands];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self prepareSplash];
            [self showSplash];
        }
    }
}

- (void)setRating
{
    NSInteger dontWaitCount = 0;
    for (NSInteger i=0; i < fetchedResultsController.fetchedObjects.count; i++) {
        Tweets *tweet = [fetchedResultsController.fetchedObjects objectAtIndex:i];
        if (tweet.dontwait.boolValue) {
            dontWaitCount++;
        }
    }
    
    bankRating1.image = [UIImage imageNamed:@"rating1"];
    bankRating2.image = [UIImage imageNamed:@"rating1"];
    bankRating3.image = [UIImage imageNamed:@"rating1"];
    bankRating4.image = [UIImage imageNamed:@"rating1"];
    bankRating5.image = [UIImage imageNamed:@"rating1"];
    
    if (dontWaitCount > bankRatingRate1) {
        bankRating5.image = [UIImage imageNamed:@"rating2"];
    }
    if (dontWaitCount > bankRatingRate2) {
        bankRating4.image = [UIImage imageNamed:@"rating2"];
    }
    if (dontWaitCount > bankRatingRate3) {
        bankRating3.image = [UIImage imageNamed:@"rating2"];
    }
    if (dontWaitCount > bankRatingRate4) {
        bankRating2.image = [UIImage imageNamed:@"rating2"];
    }
    
    bankRatingLabel.text = [NSString stringWithFormat:@"%d %@ не %@ очереди", dontWaitCount, [TimeView caseForWordByNumber:dontWaitCount variants:[NSArray arrayWithObjects:@"человек", @"человека", @"человек", nil]], [TimeView caseForWordByNumber:dontWaitCount variants:[NSArray arrayWithObjects:@"дождался", @"дождались", @"дождались", nil]]];
    
}

- (void)setBestAndBadTime
{
    if (fetchedResultsController.fetchedObjects.count == 0) {
        bankBestTime.hidden = YES;
        bankBadTime.hidden = YES;
        
        CGRect frame = headerView.frame;
        frame.size.height -= bankBestTime.frame.size.height + bankBadTime.frame.size.height;
        headerView.frame = frame;
    }
    else {
        NSMutableDictionary *timeDict = [NSMutableDictionary dictionary];
        CGFloat minimumValue = NSIntegerMax;
        NSDate *minimumDate;
        CGFloat minimumKey = -1.f;
        CGFloat maximumValue = NSIntegerMin;
        CGFloat maximumKey = -1.f;
        NSDate *maximumDate;
        
        for (NSInteger i=0; i < fetchedResultsController.fetchedObjects.count; i++) {
            Tweets *tweet = [fetchedResultsController.fetchedObjects objectAtIndex:i];
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSWeekdayCalendarUnit) fromDate:tweet.created];
            CGFloat mtowait = tweet.mtowait.floatValue;
            if (tweet.dontwait.boolValue && mtowait<1.f) {
                mtowait += 60.f;
            }
            NSString *key = [NSString stringWithFormat:@"%d.%d", components.hour, components.weekday];
            
            if ([timeDict valueForKey:key]) {
                mtowait = ([[timeDict valueForKey:key] integerValue] + mtowait) / 2;
            }
            
            [timeDict setValue:[NSNumber numberWithInteger:mtowait] forKey:key];
            
            if (mtowait < minimumValue) {
                minimumValue = mtowait;
                minimumKey = components.hour + components.weekday/10;
                minimumDate = tweet.created;
            }
            
            if (mtowait > maximumValue) {
                maximumValue = mtowait;
                maximumKey = components.hour + components.weekday/10;
                maximumDate = tweet.created;
            }
        }
        
        if (minimumKey == maximumKey) {
            if (maximumValue < 30) {
                maximumKey = -1.f;
                bankBadTime.hidden = YES;
                
                CGRect frame = headerView.frame;
                frame.size.height -= bankBadTime.frame.size.height;
                headerView.frame = frame;
            }
            else {
                minimumKey = -1.f;
                bankBestTime.hidden = YES;
                
                CGRect frame = headerView.frame;
                frame.size.height -= bankBestTime.frame.size.height;
                headerView.frame = frame;
                
                frame = bankBadTime.frame;
                frame.origin.y -= bankBestTime.frame.size.height;
                bankBadTime.frame = frame;
            }
        }
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE"];
        
        NSString *minimumKeyString = [NSString stringWithFormat:@"%d", (NSInteger)minimumKey];
        if (minimumKeyString.length == 1) {
            minimumKeyString = [NSString stringWithFormat:@"0%@", minimumKeyString];
        }
        NSString *minimumKey2String = [NSString stringWithFormat:@"%d", (NSInteger)minimumKey+1];
        if (minimumKey2String.length == 1) {
            minimumKey2String = [NSString stringWithFormat:@"0%@", minimumKey2String];
        }
        NSString *maximumKeyString = [NSString stringWithFormat:@"%d", (NSInteger)maximumKey];
        if (maximumKeyString.length == 1) {
            maximumKeyString = [NSString stringWithFormat:@"0%@", maximumKeyString];
        }
        NSString *maximumKey2String = [NSString stringWithFormat:@"%d", (NSInteger)maximumKey+1];
        if (maximumKey2String.length == 1) {
            maximumKey2String = [NSString stringWithFormat:@"0%@", maximumKey2String];
        }
        
        if (minimumKey != -1) {
            bankBestTime.text = [NSString stringWithFormat:@"%@ %@, c %@ до %@ %@", bankBestTimeText, [formatter stringFromDate:minimumDate], minimumKeyString, minimumKey2String, [TimeView caseForWordByNumber:minimumKey+1 variants:[NSArray arrayWithObjects:@"часа", @"часов", @"часов", nil]]];
        }
        
        if (maximumKey != -1) {
            bankBadTime.text = [NSString stringWithFormat:@"%@ %@, c %@ до %@ %@", bankBadTimeText, [formatter stringFromDate:maximumDate], maximumKeyString, maximumKey2String, [TimeView caseForWordByNumber:maximumKey+1 variants:[NSArray arrayWithObjects:@"часа", @"часов", @"часов", nil]]];
        }
    }
}

- (void)loadBankAvatar
{
    NSRange bankAccountRange = [bankAccount rangeOfString:@"@"];
    if (bankAccountRange.location == NSNotFound) {
        return;
    }
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (!granted) {
            return;
        }
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
        NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
        ACAccount *targetAccount;
        
        BOOL found = NO;
        for (int i=0; i < accounts.count && !found; i++) {
            targetAccount = [accounts objectAtIndex:i];
            found = ([targetAccount.username isEqualToString:userName]);
        }
        
        NSMutableString *url = [NSMutableString stringWithString:@"http://api.twitter.com/1.1/users/show.json"];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:bankAccount, @"screen_name", nil];
        NSURL *userShowUrl = [NSURL URLWithString:url];
        
        TWRequest *request = [[TWRequest alloc] initWithURL:userShowUrl parameters:params requestMethod:TWRequestMethodGET];
        [request setAccount:targetAccount];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        });
        
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse*urlResponse, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                if (!error) {
                    NSError *error;
                    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
                    if (!error) {
                        if ([dict valueForKey:@"profile_image_url"]) {
                            [headerBankAvatar setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"profile_image_url"]] placeholderImage:[UIImage imageNamed:@"angryTweetMap"]];
                        }
                    }
                }
            });
        }];
    }];
}

- (CGSize)calcHeightForText:(NSString *)str withinWidth:(CGFloat)width fontSize:(CGFloat)fontSize {
    
    CGSize textSize = {width, 20000.0};
    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:textSize];
    
    return size;
}

#pragma mark - Splash

- (void)prepareSplash
{
    hand.alpha = 0.f;
    splashView.alpha = 0.f;
    splashView.hidden = NO;
    
    CGRect frame = hand.frame;
    frame.origin.x = self.frame.size.width - frame.size.width;
    if (showHeaderView) {
        frame.origin.y += headerView.frame.size.height;
    }
    hand.frame = frame;
}

- (void)showSplash
{
    [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        splashView.alpha = 1.f;
    } completion:^(BOOL finished) {
        [self step1];
    }];
}

- (void)step1
{
     [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
         hand.alpha = 1.f;
     }
      completion:^(BOOL finished) {
          AudioServicesPlaySystemSound(1103);
          
          TweetsListCell *cell = (TweetsListCell *)[mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
          [cell performSelector:@selector(swipe) withObject:nil afterDelay:1.1];
          
          [UIView animateWithDuration:0.3 delay:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
              CGRect frame = hand.frame;
              
              frame.origin.x = self.frame.size.width/2 - frame.size.width;
              hand.frame = frame;
          }
           completion:^(BOOL finished) {
               [UIView animateWithDuration:0.2 delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                   hand.alpha = 0.f;
               }
                completion:^(BOOL finished) {
                    [self hideSplash];
                }];
           }];
      }];
}

- (void)hideSplash
{
    [UIView animateWithDuration:0.5 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        splashView.alpha = 0.f;
    }
     completion:^(BOOL finished) {
         splashView.hidden = NO;
     }];
}

- (NSString *)designateTagsInString:(NSString *)string
{
    NSMutableString *resultString = [NSMutableString string];
    BOOL allFound = NO;
    while (!allFound) {
        NSRange range = [string rangeOfString:@"#"];
        allFound = range.location == NSNotFound;
        if (range.location != NSNotFound) {
            [resultString appendFormat:@"%@<font color='#999999'>", [string substringToIndex:range.location]];
            string = [string substringFromIndex:range.location];
            range = [string rangeOfString:@" "];
            if (range.location != NSNotFound) {
                [resultString appendFormat:@"%@</font>", [string substringToIndex:range.location]];
                string = [string substringFromIndex:range.location];
                NSRange range = [string rangeOfString:@"#"];
                allFound = range.location == NSNotFound;
            }
            else {
                [resultString appendFormat:@"%@</font>", string];
                string = @"";
                allFound = YES;
            }
        }
    }
    
    [resultString appendString:string];
    
    return resultString;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [self fetchedResultsController].fetchedObjects.count;
    if (!dontShowLoadingView) {
        loadingView.hidden = count > 0;
    }
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tweets *tweet = [[self fetchedResultsController].fetchedObjects objectAtIndex:indexPath.row];
    CGSize sizeText = [self calcHeightForText:tweet.text withinWidth:self.frame.size.width-10.f fontSize:14.f];
    
    if (sizeText.height < 10.f) {
        sizeText.height = 10.f;
    }
    
    return 80.f + sizeText.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TweetsListCell";
    TweetsListCell *cell = [mainTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TweetsListCell" owner:self options:nil];
        
        for (id oneObject in nib) {
            if ([oneObject isKindOfClass:[TweetsListCell class]]) {
                cell = (TweetsListCell *)oneObject;
            }
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    Tweets *tweet = [[self fetchedResultsController].fetchedObjects objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    cell.nameLabel.text = tweet.userName;
    cell.accountLabel.text = [NSString stringWithFormat:@"@%@", tweet.userAccount];
    cell.havePosition = (tweet.locationLat && tweet.locationLong);
    if (cell.havePosition) {
        cell.tweetPosition = CLLocationCoordinate2DMake(tweet.locationLat.floatValue, tweet.locationLong.floatValue);
    }
    cell.tweetPositionButton.hidden = !cell.havePosition;
    
    // date
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEEE, d MMMM YYYY в HH:mm"];
    NSString *date = [dateFormatter stringFromDate:tweet.created];
    cell.createDateLabel.text = date;
    
    [cell.avatarImageView setImageWithURL:[NSURL URLWithString:tweet.userImageUrl] placeholderImage:[UIImage imageNamed:@"angryTweetMap"]];
    
    // tweetText
    
    NSString *tweetText = [self designateTagsInString:tweet.text];
    [cell.tweetTextWebView loadHTMLString:tweetText baseURL:nil];
    
    // stat
    
    NSString *statText = [NSString stringWithFormat:@"<b>%i %@</b> в очереди<br /><b>%@</b> ожидания<br /><b>%i %@</b> открыто<br /><b>%i %@</b> закрыто",
                          tweet.aInline.integerValue,
                          [TimeView caseForWordByNumber:tweet.aInline.integerValue variants:[NSArray arrayWithObjects:@"человек", @"человека", @"человек", nil]],
                          [TimeView timeFromMinutes:tweet.mtowait.integerValue],
                          tweet.avail.integerValue,
                          [TimeView caseForWordByNumber:tweet.avail.integerValue variants:[NSArray arrayWithObjects:@"окно", @"окна", @"окон", nil]],
                          tweet.closed.integerValue,
                          [TimeView caseForWordByNumber:tweet.closed.integerValue variants:[NSArray arrayWithObjects:@"окно", @"окна", @"окон", nil]]];
    [cell.statWebView loadHTMLString:statText baseURL:nil];
    
    // gesture
    
    if (!cell.swipeGesture) {
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeTweetCell:)];
        [cell addGestureRecognizer:swipeGestureRecognizer];
        cell.swipeGesture = swipeGestureRecognizer;
    }
    
    cell.swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    cell.swipeType = TweetsListCellSwipeTypeLeft;
    
    CGRect frame = cell.mainView.frame;
    frame.origin.x = 0.f;
    cell.mainView.frame = frame;
    
    frame = cell.statView.frame;
    frame.origin.x = cell.mainView.frame.size.width;
    cell.statView.frame = frame;
    
    return cell;
}

#pragma mark - TweetsListCelldelegate

- (void)TweetsListCell:(TweetsListCell *)cell locateTweetWithPosition:(CLLocationCoordinate2D)position
{
    [delegate TweetsListViewSetPosition:position];
    [delegate TweetsListViewDone];
}

#pragma mark - Gestures

- (void)swipeTweetCell:(UIGestureRecognizer*)gestureRec
{
    UISwipeGestureRecognizer *swipeGestureRecognizer = (UISwipeGestureRecognizer*)gestureRec;
    TweetsListCell *cell = (TweetsListCell *)swipeGestureRecognizer.view;
    [cell swipe];
}

#pragma mark - CoreData

- (NSFetchedResultsController *)fetchedResultsController
{
    if (fetchedResultsController) {
        return fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Tweets" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    if (showHeaderView) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%f < locationLat) and (locationLat < %f) and (%f < locationLong) and (locationLong < %f)", bankLatLongRange.latMin, bankLatLongRange.latMax, bankLatLongRange.longMin, bankLatLongRange.longMax];
        [fetchRequest setPredicate:predicate];
    }
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:appDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
	NSError *error = nil;
	if (![aFetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    fetchedResultsController = aFetchedResultsController;
    return fetchedResultsController;
}



@end
