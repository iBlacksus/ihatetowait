//
//  AnnotationBanksView.m
//  ihatetowait
//
//  Created by iBlacksus on 07.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "AnnotationBanksView.h"
#import "SpacialMasterController.h"
#import "ASDepthModalViewController.h"

@implementation AnnotationBanksView

@synthesize annotation;
@synthesize delegate;

- (void)initView
{
    self.frame = [SpacialMasterController childFrame];
}

- (IBAction)cancel:(id)sender
{
    [ASDepthModalViewController dismiss];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return annotation.banks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BankCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *bank = [annotation.banks objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [bank objectForKey:@"bankName"];
    cell.detailTextLabel.text = [bank objectForKey:@"bankAccount"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self cancel:nil];
    [delegate AnnotationBanksViewDoneWithTweet:[annotation.banks objectAtIndex:indexPath.row]];
}

@end
