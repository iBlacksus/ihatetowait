//
//  InfoView.h
//  ihatetowait
//
//  Created by iBlacksus on 23.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueueSizeView.h"
#import "TimeView.h"
#import "WindowsView.h"
#import "CommentView.h"

@interface InfoView : UIView

@property (nonatomic, strong) IBOutlet UILabel *bankLabel;
@property (nonatomic, strong) IBOutlet QueueSizeView *queueSizeView;
@property (nonatomic, strong) IBOutlet TimeView *timeView;
@property (nonatomic, strong) IBOutlet WindowsView *windowsView;
@property (nonatomic, strong) IBOutlet CommentView *commentView;

@end
