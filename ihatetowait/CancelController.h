//
//  CancelController.h
//  ihatetowait
//
//  Created by iBlacksus on 21.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"

@interface CancelController : MBSpacialChildViewController

- (void)spacialViewWillAppear;

@end
