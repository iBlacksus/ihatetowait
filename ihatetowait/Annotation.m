//
//  Annotation.m
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (void)dealloc {
    self.title = nil;
    self.subtitle = nil;
}

@end
