//
//  QueueSizeController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "QueueSizeController.h"
#import "SpacialMasterController.h"
#import "QueueSizeView.h"
#import "BankController.h"

@interface QueueSizeController ()
{
    QueueSizeView *mainView;
    UIAlertView *chooseBankAlertView;
}

@end

@implementation QueueSizeController

@synthesize mainViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"QueueSizeView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    mainView.bankLabel.text = [mainViewController bankNameForType:SendTweetTypeNormal];
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    if ([mainViewController bankForType:SendTweetTypeNormal].length == 0) {
        ((BankController *)self.leftViewController).needBankAlert = YES;
        [self moveInDirection:MBDirectionLeft animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tweetText
{
    NSInteger size = [mainView queueSize];
    
    if (size > 0) {
        return [NSString stringWithFormat:@"#%iinline", size];
    }
    
    return @"";
}

@end
