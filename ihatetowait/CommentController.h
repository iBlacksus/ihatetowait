//
//  CommentController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import "CommentView.h"

@interface CommentController : MBSpacialChildViewController <CommentViewDelegate>

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewWillDisappear;

@property (nonatomic) id mainViewController;

- (NSString *)tweetText;

@end
