//
//  CommentView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentViewDelegate;
@interface CommentView : UIView <UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) IBOutlet UILabel *countLabel;

@property (nonatomic) NSInteger commentMaxLength;
@property (nonatomic) id <CommentViewDelegate> delegate;

- (void)updateCountLabelForText:(NSString *)text;
- (NSString *)comment;

@end


@protocol CommentViewDelegate

- (void)commentViewDone;

@end