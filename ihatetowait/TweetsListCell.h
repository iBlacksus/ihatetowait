//
//  TweetsListCell.h
//  ihatetowait
//
//  Created by iBlacksus on 01.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    TweetsListCellSwipeTypeLeft = 0,
    TweetsListCellSwipeTypeRight
} TweetsListCellSwipeType;

@protocol TweetsListCelldelegate;
@interface TweetsListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *accountLabel;
//@property (strong, nonatomic) IBOutlet UILabel *tweetTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *createDateLabel;
@property (strong, nonatomic) IBOutlet UIButton *tweetPositionButton;
//@property (strong, nonatomic) IBOutlet UILabel *tagsLabel;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *statView;
@property (strong, nonatomic) IBOutlet UIWebView *statWebView;
@property (strong, nonatomic) IBOutlet UIWebView *tweetTextWebView;

@property (nonatomic) id <TweetsListCelldelegate> delegate;
@property (nonatomic) CLLocationCoordinate2D tweetPosition;
@property (nonatomic) BOOL havePosition;
@property (nonatomic) UISwipeGestureRecognizer *swipeGesture;
@property (nonatomic) TweetsListCellSwipeType swipeType;

- (IBAction)locateTweetPosition:(id)sender;

- (void)swipe;

@end

@protocol TweetsListCelldelegate

- (void)TweetsListCell:(TweetsListCell *)cell locateTweetWithPosition:(CLLocationCoordinate2D)position;

@end