//
//  ViewController.h
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBSpacialMasterViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocation.h>
#import "Types.h"

@interface SpacialMasterController : MBSpacialMasterViewController <CLLocationManagerDelegate, UIAlertViewDelegate>

- (NSString *)tweetTextForType:(SendTweetType)type;
- (NSString *)bankForType:(SendTweetType)type;
- (NSString *)bankNameForType:(SendTweetType)type;
- (NSInteger)tweetLengthForComment;
- (void)updateLocation;

+ (CGRect)childFrame;
+ (NSString *)nibSuffix;

@end
