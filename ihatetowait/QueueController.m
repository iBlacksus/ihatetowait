//
//  QueueController_iPhone.m
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "QueueController.h"
#import "QueueView.h"
#import "SpacialMasterController.h"

@interface QueueController ()
{
    QueueView *mainView;
}

@end

@implementation QueueController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"QueueView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    mainView.queueController = self;
    [mainView initView];
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    [mainView viewWillAppear];
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    [mainView viewDidAppear];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
