//
//  TimeController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"

@interface TimeController : MBSpacialChildViewController

- (void)spacialViewDidLoad;

- (NSString *)tweetText;

@end
