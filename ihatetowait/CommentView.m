//
//  CommentView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "CommentView.h"

@implementation CommentView

@synthesize commentTextView;
@synthesize countLabel;
@synthesize commentMaxLength;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)updateCountLabelForText:(NSString *)text
{
    NSInteger count = commentMaxLength - text.length;
    countLabel.textColor = (count >= 0) ? [UIColor darkGrayColor] : [UIColor redColor];
    
    countLabel.text = [NSString stringWithFormat:@"%d", count];
}

- (NSString *)comment
{
    if (commentTextView.text.length > commentMaxLength) {
        return [commentTextView.text substringToIndex:commentMaxLength];
    }
    
    return commentTextView.text;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [delegate commentViewDone];
        return NO;
    }
    
    NSString * proposedNewString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    [self updateCountLabelForText:proposedNewString];
    
    return YES;
}

@end
