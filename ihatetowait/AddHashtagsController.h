//
//  AddHashtagsController.h
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import "CommentController.h"
#import "AddHashtagsView.h"

@interface AddHashtagsController : MBSpacialChildViewController <AddHashtagsViewDelegate>

@property (nonatomic) CommentController *commentController;

- (void)spacialViewDidLoad;
- (NSString *)tweetTextForLength:(NSInteger)maxLength;

@end
