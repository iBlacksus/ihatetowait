//
//  SendTweetController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import "Types.h"

@interface TweetPreviewController : MBSpacialChildViewController

@property (nonatomic) SendTweetType type;
@property (nonatomic) id mainViewController;

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;

@end
