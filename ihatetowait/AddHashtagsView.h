//
//  AddHashtagsView.h
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddHashtagsViewDelegate;
@interface AddHashtagsView : UIView
{
    IBOutlet UITableView *mainTableView;
}

@property (nonatomic) id <AddHashtagsViewDelegate> delegate;

- (void)initView;
- (NSArray *)selectedHashtags;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end

@protocol AddHashtagsViewDelegate

- (void)AddHashtagsViewDone;

@end