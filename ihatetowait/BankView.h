//
//  BankView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Types.h"

@protocol BankViewDelegate;
@interface BankView : UIView <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIScrollViewDelegate, UITextFieldDelegate>
{
    IBOutlet UIView *splashView;
    IBOutlet UILabel *splashLabel;
    IBOutlet UIView *disableSearchView;
    IBOutlet UIView *newBankView;
    IBOutlet UIImageView *leftArrow;
    IBOutlet UIImageView *rightArrow;
    IBOutlet UINavigationItem *bankNavigationItem;
}

@property (nonatomic, strong) IBOutlet UITableView *mainTableView;
@property (nonatomic, strong) IBOutlet UISearchBar *mainSearchBar;
@property (nonatomic, strong) IBOutlet UITextField *bankTextField;
@property (nonatomic) NSString *bankAccount;
@property (nonatomic) NSString *bankName;
@property (nonatomic) id <BankViewDelegate> delegate;
@property (nonatomic) SendTweetType type;
@property (nonatomic) BankControllerType controllerType;

- (void)initView;
- (IBAction)cancelNewBank:(id)sender;

@end

@protocol BankViewDelegate

- (void)bankViewDone;
- (void)bankViewCancel;

@end