//
//  WindowsController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"

@interface WindowsController : MBSpacialChildViewController

- (void)spacialViewDidLoad;

- (NSString *)tweetText;

@end
