//
//  BankController.h
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import "BankView.h"
#import "Types.h"

@protocol BankContollerDelegate;
@interface BankController : MBSpacialChildViewController <BankViewDelegate>

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;
- (void)spacialViewWillDisappear;

- (NSString *)tweetText;
- (NSString *)bankName;

@property (nonatomic) SendTweetType type;
@property (nonatomic) BOOL needBankAlert;
@property (nonatomic) BankControllerType controllerType;
@property (nonatomic) id <BankContollerDelegate> delegate;

@end

@protocol BankContollerDelegate

- (void)bankControllerDoneWithBankAccount:(NSString *)bankAccount bankName:(NSString *)bankName;

@end