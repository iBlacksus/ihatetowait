//
//  QueueController_iPhone.h
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"

@interface QueueController : MBSpacialChildViewController

- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;

@end
