//
//  AnnotationBanksView.h
//  ihatetowait
//
//  Created by iBlacksus on 07.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Annotation.h"

@protocol AnnotationBanksViewDelegate;
@interface AnnotationBanksView : UIView <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *mainTableView;
}

@property (nonatomic) Annotation *annotation;
@property (nonatomic) id <AnnotationBanksViewDelegate> delegate;

- (IBAction)cancel:(id)sender;

- (void)initView;

@end

@protocol AnnotationBanksViewDelegate

- (void)AnnotationBanksViewDoneWithTweet:(NSDictionary *)tweet;

@end