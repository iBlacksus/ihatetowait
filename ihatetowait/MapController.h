//
//  MapController.h
//  ihatetowait
//
//  Created by iBlacksus on 21.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "BankMasterController.h"
#import "TweetsListView.h"
#import "AnnotationBanksView.h"
#import "EnableGeoTutorialView.h"

@interface MapController : UIViewController <UIActionSheetDelegate, AppDelegateProtocol, MKMapViewDelegate, BankMasterContollerDelegate, TweetsListViewDelegate, AnnotationBanksViewDelegate, EnableGeoTutorialViewDelegate>
{
    IBOutlet MKMapView *mainMapView;
    IBOutlet UISegmentedControl *mapSegmentControl;
    IBOutlet UIView *splashView;
    IBOutlet UILabel *splashLabel;
    IBOutlet UIBarButtonItem *optionsButton;
    IBOutlet UIImageView *questionImageView;
    IBOutlet UIView *splashGeoErrorButtons;
}

- (IBAction)add:(id)sender;
- (IBAction)options:(id)sender;
- (IBAction)changeMapType:(id)sender;
- (IBAction)setCurrentLocation:(id)sender;
- (IBAction)tweetsAround:(id)sender;
- (IBAction)geoErrorOK:(id)sender;
- (IBAction)geoErrorDontShow:(id)sender;
- (IBAction)geoErrorMore:(id)sender;

+ (LatLongRange)latLongRangeFromCoordinate:(CLLocationCoordinate2D)coordinate;

@end
