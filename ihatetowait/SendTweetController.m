//
//  SendTweetController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "SendTweetController.h"
#import "SendTweetView.h"
#import "SpacialMasterController.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "ASDepthModalViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SpacialMasterController.h"

@interface SendTweetController ()
{
    SendTweetView *mainView;
    BOOL needTweet;
    NSTimer *cancelTimer;
    TWRequest *requestWithGeo;
    BOOL needCheckGeoEnabled;
    
    UIAlertView *geoAlertView;
    UIAlertView *errorAlertView;
}

@end

@implementation SendTweetController

@synthesize location;
@synthesize type;
@synthesize tweetText;
@synthesize mainViewController;
@synthesize locationLat;
@synthesize locationLong;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"SendTweetView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    mainView.cancelView.hidden = YES;
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    self.panGesture.enabled = NO;
    cancelTimer = [NSTimer scheduledTimerWithTimeInterval:10.f target:self selector:@selector(showCancelView) userInfo:nil repeats:NO];
    
    needTweet = NO;
    needCheckGeoEnabled = YES;
    [self checkGeoEnabledAndTweet];
}

- (void)spacialViewDidDisappear
{
    [super spacialViewDidDisappear];
    
    [cancelTimer invalidate];
    cancelTimer = nil;
    mainView.cancelView.hidden = YES;
    requestWithGeo = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showCancelView
{
    [cancelTimer invalidate];
    cancelTimer = nil;
    
    mainView.cancelView.hidden = NO;
    self.panGesture.enabled = YES;
}

- (void)locationUpdated
{
    if (needCheckGeoEnabled) {
        [self checkGeoEnabledAndTweet];
    }
    else if (needTweet) {
        [self tweet];
    }
}

- (void)checkGeoEnabledAndTweet
{
    needCheckGeoEnabled = NO;
    
    BOOL dontCheckGeoEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:udDontCheckGeoEnabled];
    if (dontCheckGeoEnabled) {
        needTweet = YES;
        [self tweet];
        return;
    }
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (!granted) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                errorAlertView = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Приложение не может получить доступ к аккаунтам Twitter. Проверьте настройки Twitter в настройках Вашего устройства, а так же разрешите доступ приложению." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlertView show];
            });
            return;
        }
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
        NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
        ACAccount *targetAccount;
        
        BOOL found = NO;
        for (int i=0; i < accounts.count && !found; i++) {
            targetAccount = [accounts objectAtIndex:i];
            found = ([targetAccount.username isEqualToString:userName]);
        }
        
        NSURL *statusUpdateUrl = [NSURL URLWithString:@"http://api.twitter.com/1.1/users/show.json"];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:userName, @"screen_name", nil];
        
        requestWithGeo = [[TWRequest alloc] initWithURL:statusUpdateUrl parameters:params requestMethod:TWRequestMethodGET];
        [requestWithGeo setAccount:targetAccount];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        });
        
        [requestWithGeo performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse*urlResponse, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                
                NSError *error;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    return;
                }
                
                if ([dict valueForKey:@"geo_enabled"] && ![[dict valueForKey:@"geo_enabled"] boolValue]) {
                    geoAlertView = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Чтобы прикрепить к твиту свое местоположение, Вам нужно активировать опцию \"Указывать, где я нахожусь\" в настройках Вашего твиттер-аккаунта." delegate:self cancelButtonTitle:@"Все равно отправить" otherButtonTitles:@"OK", @"Больше не напоминать", nil];
                    [geoAlertView show];
                    return;
                }
                
                needTweet = YES;
                [self tweet];
                
            });
        }];
    }];
}

- (void)tweet
{
    if (!location) {
        [(SpacialMasterController *)mainViewController updateLocation];
        return;
    }
    
    needTweet = NO;
    
    //  hold on to the ACAccountStore
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (!granted) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                errorAlertView = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Приложение не может получить доступ к аккаунтам Twitter. Проверьте настройки Twitter в настройках Вашего устройства, а так же разрешите доступ приложению." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlertView show];
            });
            return;
        }
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
        NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
        ACAccount *targetAccount;
        
        BOOL found = NO;
        for (int i=0; i < accounts.count && !found; i++) {
            targetAccount = [accounts objectAtIndex:i];
            found = ([targetAccount.username isEqualToString:userName]);
        }
        
        NSURL *statusUpdateUrl = [NSURL URLWithString:@"http://api.twitter.com/1.1/statuses/update.json"];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tweetText, @"status", @"true", @"display_coordinates",
#ifdef DEBUG
#warning locationLat locationLong
                                //locationLat, @"lat", locationLong, @"long",
                                @"55.755520", @"lat", @"37.618217", @"long",
#else
                                [NSString stringWithFormat:@"%f", location.coordinate.latitude], @"lat",
                                [NSString stringWithFormat:@"%f", location.coordinate.longitude], @"long",
#endif
                                nil];
        
        requestWithGeo = [[TWRequest alloc] initWithURL:statusUpdateUrl parameters:params requestMethod:TWRequestMethodPOST];
        [requestWithGeo setAccount:targetAccount];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        });
        
        [requestWithGeo performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse*urlResponse, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
            
                AudioServicesPlaySystemSound(1016);
                self.panGesture.enabled = YES;
                [ASDepthModalViewController dismiss];
            });
        }];
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == errorAlertView) {
        self.panGesture.enabled = YES;
        [self moveInDirection:MBDirectionDown animated:YES];
    }
    else if (alertView == geoAlertView) {
        //не напоминать
        if (buttonIndex == 2) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udDontCheckGeoEnabled];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if ((buttonIndex == 1) || (buttonIndex == 2)) {
            self.panGesture.enabled = YES;
            [self moveInDirection:MBDirectionDown animated:YES];
        }
        
        // все равно отправить
        if (buttonIndex == 0) {
            needTweet = YES;
            [self tweet];
        }
        
    }
    
}

@end