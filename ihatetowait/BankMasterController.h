//
//  BankMasterController.h
//  ihatetowait
//
//  Created by iBlacksus on 26.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialMasterViewController.h"
#import "BankController.h"

@protocol BankMasterContollerDelegate;
@interface BankMasterController: MBSpacialMasterViewController <BankContollerDelegate>

@property (nonatomic) id <BankMasterContollerDelegate> delegate;

@end

@protocol BankMasterContollerDelegate

- (void)bankMasterControllerDoneWithBankAccount:(NSString *)bankAccount bankName:(NSString *)bankName;

@end