//
//  CancelController.m
//  ihatetowait
//
//  Created by iBlacksus on 21.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "CancelController.h"
#import "ASDepthModalViewController.h"

@interface CancelController ()

@end

@implementation CancelController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    [ASDepthModalViewController dismiss];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
