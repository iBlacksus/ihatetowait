//
//  WindowsView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WindowsViewDelegate;
@interface WindowsView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIPickerView *windowsPicker;
}

@property (nonatomic) id <WindowsViewDelegate> delegate;

- (NSInteger)openedWindows;
- (NSInteger)closedWindows;

@end

@protocol WindowsViewDelegate

- (void)windowsViewValueChanged;

@end