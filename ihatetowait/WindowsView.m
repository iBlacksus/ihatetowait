//
//  WindowsView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "WindowsView.h"

#define numberOfRows 100

@implementation WindowsView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSInteger)openedWindows
{
    NSInteger selectedRow = [windowsPicker selectedRowInComponent:1];
    return selectedRow;
}

- (NSInteger)closedWindows
{
    NSInteger selectedRow = [windowsPicker selectedRowInComponent:0];
    return selectedRow;
}

#pragma mark - UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return numberOfRows;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%i", row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [delegate windowsViewValueChanged];
}

@end
