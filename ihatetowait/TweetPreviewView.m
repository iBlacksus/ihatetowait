//
//  SendTweetView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TweetPreviewView.h"

@implementation TweetPreviewView

@synthesize sendTweetController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)setLatLong:(id)sender
{
    sendTweetController.locationLat = locationLat.text;
    sendTweetController.locationLong = locationLong.text;
}

@end
