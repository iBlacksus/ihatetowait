//
//  AddHashtagsView.m
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "AddHashtagsView.h"

@implementation AddHashtagsView
{
    NSArray *hashtags;
}

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initView
{
    [self loadHashtags];
}

- (void)loadHashtags
{
    NSData *data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/hashtags.json", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    hashtags = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось загрузить список хэштегов" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"tag" ascending:YES];
    hashtags = [hashtags sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
    
    [mainTableView reloadData];
}

- (IBAction)cancel:(id)sender
{
    NSArray *indexPaths = mainTableView.indexPathsForSelectedRows;
    
    for (NSInteger i=0; i < indexPaths.count; i++) {
        [mainTableView deselectRowAtIndexPath:[indexPaths objectAtIndex:i] animated:YES];
    }
}

- (IBAction)done:(id)sender
{
    [delegate AddHashtagsViewDone];
}

- (NSArray *)selectedHashtags
{
    NSMutableArray *tags = [NSMutableArray array];
    NSArray *indexPaths = mainTableView.indexPathsForSelectedRows;
    
    for (NSInteger i=0; i < indexPaths.count; i++) {
        NSDictionary *tag = [hashtags objectAtIndex:[[indexPaths objectAtIndex:i] row]];
        [tags addObject:[NSString stringWithFormat:@"#%@", [tag valueForKey:@"tag"]]];
    }
    
    return tags;
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return hashtags.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"HashtagCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *tag = [hashtags objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"#%@", [tag valueForKey:@"tag"]];
    
    return cell;
}

@end
