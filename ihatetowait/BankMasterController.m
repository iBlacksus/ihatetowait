//
//  BankMasterController.m
//  ihatetowait
//
//  Created by iBlacksus on 26.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "BankMasterController.h"
#import "Types.h"

@interface BankMasterController ()
{
    BankController *bank;
}

@end

@implementation BankMasterController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.hasMap = NO;
    self.view.backgroundColor = [UIColor clearColor];
    bank = [[BankController alloc] init];
    bank.delegate = self;
    bank.controllerType = BankControllerTypeModal;
    bank.panGesture.enabled = NO;
    self.root = bank;
    // не вызывается автоматически !!
    [bank spacialViewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // не вызывается автоматически !!
    [bank spacialViewWillAppear];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // не вызывается автоматически !!
    [bank spacialViewDidAppear];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BankContollerDelegate

- (void)bankControllerDoneWithBankAccount:(NSString *)bankAccount bankName:(NSString *)bankName
{
    if (delegate) {
        [delegate bankMasterControllerDoneWithBankAccount:bankAccount bankName:bankName];
    }
}

@end
