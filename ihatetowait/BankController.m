//
//  BankController.m
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "BankController.h"
#import "SpacialMasterController.h"
#import <AudioToolbox/AudioToolbox.h>

typedef enum {
    BankViewFirstResponderTypeSearchBar = 0,
    BankViewFirstResponderTypeBankTextField
} BankViewFirstResponderType;

@interface BankController ()
{
    BankView *mainView;
    BankViewFirstResponderType firstResponderType;
    MBSpacialChildViewController *childController;
    // оптимизация для быстрого переключение на этот экран (тормозит показ клавиатуры первый раз)
    BOOL firstAppear;
    UIAlertView *bankAlertView;
}

@end

@implementation BankController

@synthesize type;
@synthesize needBankAlert;
@synthesize controllerType;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"BankView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    if (controllerType == BankControllerTypeModal) {
        CGRect frame = mainView.frame;
        frame.origin.y += 10.f;
        mainView.frame = frame;
    }
    mainView.delegate = self;
    mainView.type = type;
    mainView.controllerType = controllerType;
    [self.view addSubview:mainView];
    [mainView initView];
    firstAppear = YES;
    needBankAlert = NO;
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    if (firstAppear) {
        return;
    }
    
    [self setFirstResponder];
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    if (needBankAlert) {
        [self showBankAlert];
    }
    
    if (!firstAppear) {
        return;
    }
    
    firstAppear = NO;
    
    [self setFirstResponder];
}

- (void)spacialViewWillDisappear
{
    if (mainView.mainSearchBar.isFirstResponder) {
        firstResponderType = BankViewFirstResponderTypeSearchBar;
        [mainView.mainSearchBar resignFirstResponder];
    }
    else if (mainView.bankTextField.isFirstResponder) {
        firstResponderType = BankViewFirstResponderTypeBankTextField;
        [mainView.bankTextField resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showBankAlert
{
    needBankAlert = NO;
    
    bankAlertView = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [bankAlertView show];
    
    CGRect frame = bankAlertView.frame;
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, 150.f, 50.f)];
    title.center = CGPointMake(frame.size.width/2-10, frame.size.height/2-10);
    title.backgroundColor = [UIColor clearColor];
    title.font = [UIFont fontWithName:@"Helvetica-Bold" size:18.f];
    title.textColor = [UIColor whiteColor];
    title.text = @"Выберите банк";
    [bankAlertView addSubview:title];
    
    AudioServicesPlaySystemSound(4095);
    
    [self performSelector:@selector(hideBankAlertView) withObject:nil afterDelay:2.0];
}

- (void)hideBankAlertView
{
    [bankAlertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)setFirstResponder
{
    if (firstResponderType == BankViewFirstResponderTypeSearchBar) {
        [mainView.mainSearchBar becomeFirstResponder];
    }
    else if (firstResponderType == BankViewFirstResponderTypeBankTextField) {
        [mainView.bankTextField becomeFirstResponder];
    }
}

- (NSString *)tweetText
{
    return mainView.bankAccount;
}

- (NSString *)bankName
{
    return mainView.bankName;
}

#pragma mark - BankViewDelegate

- (void)bankViewDone
{
    if (controllerType == BankControllerTypeModal) {
        if (delegate) {
            [delegate bankControllerDoneWithBankAccount:mainView.bankAccount bankName:mainView.bankName];
        }
    }
    else if (controllerType == BankControllerTypeNormal) {
        if (type == SendTweetTypeDontWait) {
            [self moveInDirection:MBDirectionLeft animated:YES];
        }
        else if (type == SendTweetTypeNormal) {
            [self moveInDirection:MBDirectionRight animated:YES];
        }
    }
}

- (void)bankViewCancel
{
    if (controllerType == BankControllerTypeModal) {
        [mainView.bankTextField resignFirstResponder];
        if (delegate) {
            [delegate bankControllerDoneWithBankAccount:nil bankName:nil];
        }
    }
}

@end
