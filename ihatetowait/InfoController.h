//
//  InfoController.h
//  ihatetowait
//
//  Created by iBlacksus on 23.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import "CommentView.h"
#import "QueueSizeView.h"
#import "TimeView.h"
#import "WindowsView.h"

@interface InfoController : MBSpacialChildViewController <CommentViewDelegate, QueueSizeViewDelegate, TimeViewDelegate, WindowsViewDelegate>

@property (nonatomic) id mainViewController;

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;
- (void)spacialViewWillDisappear;

- (NSString *)tweetText;

@end
