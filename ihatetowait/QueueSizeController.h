//
//  QueueSizeController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"

@interface QueueSizeController : MBSpacialChildViewController

@property (nonatomic) id mainViewController;

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;
- (NSString *)tweetText;

@end
