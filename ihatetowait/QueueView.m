//
//  QueueView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "QueueView.h"
#import <AudioToolbox/AudioToolbox.h>

#define udShowingQueueHands @"showingQueueHands"

@implementation QueueView
{
    BOOL showSplash;
}

@synthesize queueController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initView
{
    yesView.backgroundColor = [UIColor clearColor];
    noView.backgroundColor = [UIColor clearColor];
    cancelView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear
{
    showSplash = ![[NSUserDefaults standardUserDefaults] boolForKey:udShowingQueueHands];
    if (showSplash) {
        [self prepareSplash];
    }
}

- (void)viewDidAppear
{
    if (showSplash) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udShowingQueueHands];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showSplash];
    }
}

- (IBAction)help:(id)sender
{
    [self prepareSplash];
    [self showSplash];
}

#pragma mark - Splash

- (void)prepareSplash
{
    queueController.panGesture.enabled = NO;
    
    yesView.alpha = 0.f;
    noView.alpha = 0.f;
    cancelView.alpha = 0.f;
    leftHand.alpha = 0.f;
    rightHand.alpha = 0.f;
    splashView.alpha = 0.f;
    splashView.hidden = NO;
    
    CGRect frame = rightHand.frame;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        frame.origin = CGPointMake(245.f, yesView.frame.origin.y + 56.f);
    }
    else {
        frame.origin = CGPointMake(566.f, yesView.frame.origin.y + 95.f);
    }
    rightHand.frame = frame;
    frame = leftHand.frame;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        frame.origin = CGPointMake(37.f, noView.frame.origin.y + 56.f);
    }
    else {
        frame.origin = CGPointMake(95.f, noView.frame.origin.y + 95.f);
    }
    leftHand.frame = frame;
}

- (void)showSplash
{
    [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        splashView.alpha = 1.f;
    } completion:^(BOOL finished) {
        [self step1];
    }];
}

- (void)step1
{
    [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        yesView.alpha = 1.f;
    }
    completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            rightHand.alpha = 1.f;
        }
        completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 delay:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                CGRect frame = rightHand.frame;
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    frame.origin.x -= 80.f;
                }
                else {
                    frame.origin.x -= 130.f;
                }
                rightHand.frame = frame;
                AudioServicesPlaySystemSound(1103);
            }
            completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5 delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    rightHand.alpha = 0.f;
                }
                completion:^(BOOL finished) {
                    [self step2];
                }];
            }];
        }];
    }];
}

- (void)step2
{
    [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        noView.alpha = 1.f;
    }
    completion:^(BOOL finished) {
         [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
             leftHand.alpha = 1.f;
         }
         completion:^(BOOL finished) {
             [UIView animateWithDuration:0.5 delay:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                 CGRect frame = leftHand.frame;
                 if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {  
                     frame.origin.x += 80.f;
                 }
                 else {
                     frame.origin.x += 180.f;
                 }
                 leftHand.frame = frame;
                 AudioServicesPlaySystemSound(1103);
             }
             completion:^(BOOL finished) {
                   [UIView animateWithDuration:0.5 delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                       leftHand.alpha = 0.f;
                   }
                   completion:^(BOOL finished) {
                        [self step3];
                   }];
            }];
        }];
    }];
}

- (void)step3
{
    [UIView animateWithDuration:0.5 delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        cancelView.alpha = 1.f;
        splashView.alpha = 0.f;
    }
    completion:^(BOOL finished) {
        splashView.hidden = NO;
        queueController.panGesture.enabled = YES;
    }];
}

#pragma mark -

- (IBAction)play:(id)sender
{
    AudioServicesPlaySystemSound(textField.text.integerValue);
}

@end
