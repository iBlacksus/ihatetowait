//
//  AddHashtagsController.m
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "AddHashtagsController.h"
#import "SpacialMasterController.h"

@interface AddHashtagsController ()
{
    AddHashtagsView *mainView;
}

@end

@implementation AddHashtagsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"AddHashtagsView_iPhone"] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    mainView.delegate = self;
    [mainView initView];
    [self.view addSubview:mainView];
}

- (NSString *)tweetTextForLength:(NSInteger)maxLength
{
    NSArray *tags = [mainView selectedHashtags];
    NSMutableString *result = [NSMutableString string];
    
    // !! проход по всему массиву, если один их тегов слишком длинный, возможно подойдет другой
    for (NSInteger i=0; i < tags.count; i++) {
        NSString *tag = [tags objectAtIndex:i];
        if (result.length + tag.length + 1 <= maxLength) {
            if (result.length > 0) {
                [result appendString:@" "];
            }
            [result appendString:tag];
        }
    }
    
    return result;
}

#pragma mark - AddHashtagsViewDelegate

- (void)AddHashtagsViewDone
{
    [self moveInDirection:MBDirectionUp animated:YES];
}

@end
