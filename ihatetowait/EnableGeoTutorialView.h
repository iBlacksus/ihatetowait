//
//  EGTStep1Controller.h
//  ihatetowait
//
//  Created by iBlacksus on 11.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EnableGeoTutorialViewDelegate;
@interface EnableGeoTutorialView : UIView
{
    IBOutlet UILabel *twitterAccountUrlLabel;
}

@property (nonatomic) id <EnableGeoTutorialViewDelegate> delegate;

- (IBAction)done:(id)sender;
- (IBAction)copyLink:(id)sender;
- (IBAction)open:(id)sender;

- (void)initView;

@end

@protocol EnableGeoTutorialViewDelegate

- (void)enableGeoTutorialViewDone;

@end