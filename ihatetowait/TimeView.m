//
//  TimeView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TimeView.h"

#define numberOfRows 96

@implementation TimeView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (NSString *)caseForWordByNumber:(NSInteger)number variants:(NSArray *)variants
{
    if ((number >= 10) && (number <= 20)) {
        return [variants objectAtIndex:2];
    }
    
    while (number > 10) {
        number = number % 10;
    }
    
    if (number == 1) {
        return [variants objectAtIndex:0];
    }
    
    if ((number >= 2) && (number <= 4)) {
        return [variants objectAtIndex:1];
    }
    
    return [variants objectAtIndex:2];
}

- (NSInteger)minutes
{
    NSInteger selectedRow = [timePicker selectedRowInComponent:0];
    return [self minutesForRow:selectedRow];
}

- (NSInteger)minutesForRow:(NSInteger)row
{
    return row * 5;
}

+ (NSString *)timeFromMinutes:(NSInteger)minutes
{
    NSInteger hours = 0;
    NSMutableString *time = [NSMutableString string];
    
    if (minutes > 59) {
        hours = minutes / 60;
        minutes -= hours * 60;
    }
    
    if (hours > 0) {
        [time appendFormat:@"%i %@ ", hours, [self caseForWordByNumber:hours variants:[NSArray arrayWithObjects:@"час", @"часа", @"часов", nil]]];
    }
    
    if ((minutes > 0) || (hours == 0)) {
        [time appendFormat:@"%i минут", minutes];
    }
    
    
    return time;
}

#pragma mark - UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return numberOfRows;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [TimeView timeFromMinutes:[self minutesForRow:row]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [delegate timeViewValueChanged];
}

@end
