//
//  QueueSizeView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QueueSizeViewDelegate;
@interface QueueSizeView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIPickerView *queuePicker;
}

@property (nonatomic, strong) IBOutlet UILabel *bankLabel;
@property (nonatomic) id <QueueSizeViewDelegate> delegate;

- (NSInteger)queueSize;

@end

@protocol QueueSizeViewDelegate

- (void)queueSizeViewValueChanged;

@end