//
//  WindowsController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "WindowsController.h"
#import "SpacialMasterController.h"
#import "WindowsView.h"

@interface WindowsController ()
{
    WindowsView *mainView;
}

@end

@implementation WindowsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"WindowsView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    [self.view addSubview:mainView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tweetText
{
    NSInteger openedWindows = [mainView openedWindows];
    NSInteger closedWindows = [mainView closedWindows];
    NSMutableString *text = [NSMutableString string];
    
    if (openedWindows > 0) {
        [text appendFormat:@"#%iavail", openedWindows];
    }
    if (closedWindows > 0) {
        if (text.length > 0) {
            [text appendString:@" "];
        }
        [text appendFormat:@"#%iclosed", closedWindows];
    }
    
    return text;
}

@end
