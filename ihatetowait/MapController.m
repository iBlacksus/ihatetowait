//
//  MapController.m
//  ihatetowait
//
//  Created by iBlacksus on 21.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MapController.h"
#import "SpacialMasterController.h"
#import "ASDepthModalViewController.h"
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "Types.h"
#import "Tweets.h"
#import "Annotation.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define oacShowBanksIndex 0
#define oacShowBanksTitle @"Список банков"
#define oacTweetsAroundIndex 1
#define oacTweetsAroundTitle @"Банки вокруг меня"
#define oacCurrentPositionIndex 2
#define oacCurrentPositionTitle @"Текущее местоположение"
#define oacChangeAccountIndex 3
#define oacChangeAccountTitle @"Сменить Twitter-аккаунт"

#define tweetsCount 100

#define latLongRound 10000

@interface MapController ()
{
    UIActionSheet *twitterActionSheet;
    UIActionSheet *optionsActionSheet;
    UIActionSheet *geoErrorActionSheet;
    NSArray *twitterAccounts;
    BOOL needAccount;
    BOOL needGeoTweets;
    NSString *nextTweetResults;
    AppDelegate *appDelegate;
    TweetsListView *tweetsListView;
    //AnnotationBanksView *annotationBanksView;
    BOOL needTweetsListViewDone;
    NSMutableArray *requests;
    
    BOOL geoViewVisible;
}

@end

@implementation MapController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *account = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
#if (TARGET_IPHONE_SIMULATOR)
    account = @"iHateToWaitApp";
#endif
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.delegate = self;
    
	// Do any additional setup after loading the view.
    needAccount = account.length == 0;
    
    if (needAccount) {
        splashLabel.hidden = NO;
        [self loginToTwitter];
    }
    else {
        [self checkTwitterAccess];
    }
    
    [mainMapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    /*if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        questionImageView.image = [UIImage imageNamed:@"question_iPad"];
        CGRect frame = questionImageView.frame;
        frame.size = CGSizeMake(510.f, 442.f);
        frame.origin.y = self.view.frame.size.height - frame.size.height;
        questionImageView.frame = frame;
        
        frame = splashGeoErrorButtons.frame;
        frame.origin.y += 
    }*/
    
    requests = [NSMutableArray array];
    needGeoTweets = YES;
    needTweetsListViewDone = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    mainMapView = nil;
    mapSegmentControl = nil;
    splashView = nil;
    splashLabel = nil;
    optionsButton = nil;
    questionImageView = nil;
    splashGeoErrorButtons = nil;
    [super viewDidUnload];
}

#pragma mark - Twitter

- (void)checkTwitterAccess
{
#if (TARGET_IPHONE_SIMULATOR)
    [self hideSplash];
    return;
#endif
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted) {
            [self checkGeoEnabledAndTweet];
            //[self performSelectorOnMainThread:@selector(hideSplash) withObject:nil waitUntilDone:NO];
        }
        else {
            [self performSelectorOnMainThread:@selector(showTwitterAccessError) withObject:nil waitUntilDone:NO];
        }
    }];
}

- (void)checkGeoEnabledAndTweet
{
    BOOL dontCheckGeoEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:udDontCheckGeoEnabled];
    if (dontCheckGeoEnabled) {
        [self hideSplash];
        return;
    }
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (!granted) {
            [self hideSplash];
            return;
        }
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
        NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
        ACAccount *targetAccount;
        
        BOOL found = NO;
        for (int i=0; i < accounts.count && !found; i++) {
            targetAccount = [accounts objectAtIndex:i];
            found = ([targetAccount.username isEqualToString:userName]);
        }
        
        NSURL *statusUpdateUrl = [NSURL URLWithString:@"http://api.twitter.com/1.1/users/show.json"];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:userName, @"screen_name", nil];
        
        TWRequest *requestWithGeo = [[TWRequest alloc] initWithURL:statusUpdateUrl parameters:params requestMethod:TWRequestMethodGET];
        [requestWithGeo setAccount:targetAccount];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        });
        
        [requestWithGeo performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse*urlResponse, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                if (error) {
                    [self hideSplash];
                    return;
                }
                
                NSError *error;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    [self hideSplash];
                    return;
                }
                
                if ([dict valueForKey:@"geo_enabled"] && ![[dict valueForKey:@"geo_enabled"] boolValue]) {
                    [self showGeoEnabledError];
                }
                else {
                    [self hideSplash];
                }
                
            });
        }];
    }];
}

- (void)loginToTwitter
{
    [self setSplashLabelForChooseAccount];
    if (splashView.hidden) {
        [self showSplash];
    }
    else {
        splashView.alpha = 1.f;
    }
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted) {
            twitterAccounts = [accountStore accountsWithAccountType:accountType];
            [self performSelectorOnMainThread:@selector(showTwitterAccounts) withObject:nil waitUntilDone:NO];
        }
        else {
            [self performSelectorOnMainThread:@selector(showTwitterAccessError) withObject:nil waitUntilDone:NO];
        }
    }];
}

- (void)setSplashLabelForChooseAccount
{
    splashLabel.text = @"Какой Twitter-аккаунт Вы хотите использовать?";
}

- (void)showTwitterAccessError
{
#if (TARGET_IPHONE_SIMULATOR)
    return;
#endif
    
    if (splashView.hidden) {
        [self showSplash];
    }
    else {
        splashView.alpha = 1.f;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        splashGeoErrorButtons.hidden = YES;
    }
    splashLabel.text = @"Приложение не может получить доступ к аккаунтам Twitter. Проверьте настройки Twitter в настройках Вашего устройства, а так же разрешите доступ приложению.";
    splashLabel.hidden = NO;
    
    [ASDepthModalViewController dismiss];
}

- (void)showGeoEnabledError
{
    if (splashView.hidden) {
        [self showSplash];
    }
    else {
        splashView.alpha = 1.f;
    }
    
    splashLabel.text = @"Чтобы указать из какого отделения банка Вы отправляете сообщение, Вам нужно прикрепить к твиту свое местоположение. Активируйте опцию \"Указывать, где я нахожусь\" в настройках Вашего твиттер-аккаунта.";
    splashLabel.hidden = NO;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        splashGeoErrorButtons.hidden = NO;
    }
    else {
        [self showGeoErrorActionSheet];
    }
}

-(void)showTwitterAccounts
{
    NSMutableArray *buttonsArray = [NSMutableArray array];
    [twitterAccounts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [buttonsArray addObject:((ACAccount*)obj).username];
    }];
    
    twitterActionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите Twitter-аккаунт" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    for (NSString *title in buttonsArray) {
        [twitterActionSheet addButtonWithTitle:title];
    }
    [twitterActionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet == twitterActionSheet) {
        if (actionSheet.cancelButtonIndex == buttonIndex) {
            [self loginToTwitter];
            return;
        }
        
        NSString *username = [actionSheet buttonTitleAtIndex:buttonIndex];
        [[NSUserDefaults standardUserDefaults] setValue:username forKey:udTwitterAccount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        needAccount = NO;
        [self checkTwitterAccess];
    }
    else if (actionSheet == optionsActionSheet) {
        if (actionSheet.cancelButtonIndex == buttonIndex) {
            return;
        }
        
        switch (buttonIndex) {
            case oacCurrentPositionIndex: {
                [self setCurrentLocation:nil];
                break;
            }
            case oacShowBanksIndex: {
                if (!needAccount) {
                    BankMasterController *controller = [[BankMasterController alloc] init];
                    controller.delegate = self;
                    [ASDepthModalViewController presentView:controller.view withBackgroundColor:nil popupAnimationStyle:ASDepthModalAnimationShrink blur:YES];
                }
                break;
            }
            case oacTweetsAroundIndex: {
                [self tweetsAround:nil];
                break;
            }
            case oacChangeAccountIndex: {
#if (TARGET_IPHONE_SIMULATOR)
                break;
#endif
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:udTwitterAccount];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self setSplashLabelForChooseAccount];
                needAccount = YES;
                splashLabel.hidden = NO;
                //[self showSplash];
                [self loginToTwitter];
                break;
            }
        }
    }
    else if (actionSheet == geoErrorActionSheet) {
        if (actionSheet.cancelButtonIndex == buttonIndex) {
            [self showGeoErrorActionSheet];
            return;
        }
        
        switch (buttonIndex) {
            case 0: {
                [self geoErrorOK:nil];
                break;
            }
            case 1: {
                [self geoErrorMore:nil];
                break;
            }
            case 2: {
                [self geoErrorDontShow:nil];
                break;
            }
        }
    }
}

#pragma mark - GEO Error Buttons

- (IBAction)geoErrorOK:(id)sender
{
    [self hideSplash];
    [self checkTwitterAccess];
}

- (IBAction)geoErrorDontShow:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udDontCheckGeoEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self hideSplash];
}

- (IBAction)geoErrorMore:(id)sender
{
    EnableGeoTutorialView *enableGeoTutorialView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"EnableGeoTutorialView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    enableGeoTutorialView.delegate = self;
    [enableGeoTutorialView initView];
    [ASDepthModalViewController presentView:enableGeoTutorialView withBackgroundColor:nil popupAnimationStyle:ASDepthModalAnimationShrink blur:YES];
    geoViewVisible = YES;
}

#pragma mark -

- (void)hideSplash
{
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         splashView.alpha = 0.f;
                     }
                     completion:^(BOOL finished) {
                         splashView.hidden = YES;
                         if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                             splashGeoErrorButtons.hidden = YES;
                         }
                     }
     ];
}

- (void)showSplash
{
    splashView.hidden = NO;
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         splashView.alpha = 1.f;
                     }
                     completion:nil
     ];
}

- (IBAction)add:(id)sender
{
    if (needAccount) {
        return;
    }
    
    SpacialMasterController *controller = [[SpacialMasterController alloc] init];
    CGRect frame = controller.view.frame;
    frame.origin.y = 10.f;
    controller.view.frame = frame;
    [ASDepthModalViewController presentView:controller.view withBackgroundColor:nil popupAnimationStyle:ASDepthModalAnimationShrink blur:YES];
}

- (IBAction)options:(id)sender
{
    if (needAccount) {
        return;
    }
    
    optionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:oacShowBanksTitle, oacTweetsAroundTitle, oacCurrentPositionTitle, oacChangeAccountTitle, nil];
    [optionsActionSheet showFromBarButtonItem:optionsButton animated:YES];
}

- (void)showGeoErrorActionSheet
{
    if (splashView.hidden || geoViewVisible) {
        return;
    }
    
    geoErrorActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Готово", @"Подробнее", @"Больше не показывать", nil];
    [geoErrorActionSheet showInView:self.view];
}

- (IBAction)changeMapType:(id)sender
{
    switch (mapSegmentControl.selectedSegmentIndex) {
        case 0:
            mainMapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            mainMapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            mainMapView.mapType = MKMapTypeHybrid;
            break;
    }
}

- (IBAction)setCurrentLocation:(id)sender
{
#ifdef DEBUG
    [mainMapView setCenterCoordinate:CLLocationCoordinate2DMake(55.782079, 37.698720) animated:YES];
#else
    [mainMapView setCenterCoordinate:mainMapView.userLocation.location.coordinate animated:YES];
#endif
}

- (IBAction)tweetsAround:(id)sender
{
    needTweetsListViewDone = NO;
    nextTweetResults = nil;
    [self cancelAllRequests];
    [self removeAllTweets];
    [self loadTweetsWithGeocode];
    [self setCurrentLocation:sender];
}

// безопасное преобразование числа из строки !!
- (NSNumber *)numberFromString:(NSString *)string
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter numberFromString:string];
}

- (NSString *)removeSubstring:(NSString *)subString fromString:(NSString *)string
{
    NSRange range = [string rangeOfString:subString];
    if (range.location != NSNotFound) {
        string = [NSString stringWithFormat:@"%@%@", [string substringToIndex:range.location], [string substringFromIndex:range.location+range.length]];
    }
    
    return string;
}

- (NSArray *)addTweets:(NSArray *)tweetsArray
{
    NSMutableArray *ids = [NSMutableArray array];
    
    for (NSInteger i=0; i < tweetsArray.count; i++) {
        NSDictionary *tweet = [tweetsArray objectAtIndex:i];
        
        [ids addObject:[tweet valueForKey:@"id"]];
        BOOL needBankName = YES;
        
        NSString *tweetFullText = [tweet valueForKey:@"text"];
        tweetFullText = [self removeSubstring:mainTweetTag fromString:tweetFullText];
        
        Tweets *newTweet = [NSEntityDescription insertNewObjectForEntityForName:@"Tweets" inManagedObjectContext:appDelegate.managedObjectContext];
        newTweet.webID = [NSNumber numberWithLongLong:[[tweet valueForKey:@"id"] longLongValue]];
        newTweet.dontwait = [NSNumber numberWithBool:NO];
        
        if ([tweet valueForKey:@"coordinates"] && [tweet valueForKey:@"coordinates"] != [NSNull null]) {
            newTweet.locationLong = [NSString stringWithFormat:@"%@", [[[tweet valueForKey:@"coordinates"] valueForKey:@"coordinates"] objectAtIndex:0]];
            newTweet.locationLat = [NSString stringWithFormat:@"%@", [[[tweet valueForKey:@"coordinates"] valueForKey:@"coordinates"] objectAtIndex:1]];
        }
        
        NSArray *userMentions = [[tweet valueForKey:@"entities"] valueForKey:@"user_mentions"];
        if (userMentions.count > 0) {
            newTweet.bankAccount = [[userMentions objectAtIndex:0] valueForKey:@"screen_name"];
            newTweet.bankName = [[userMentions objectAtIndex:0] valueForKey:@"name"];
            needBankName = NO;
            tweetFullText = [self removeSubstring:[NSString stringWithFormat:@"@%@", newTweet.bankAccount] fromString:tweetFullText];
        }
        else {
            NSString *tweetText = [tweet valueForKey:@"text"];
            NSRange range = [tweetText rangeOfString:@"@"];
            if (range.location != NSNotFound) {
                tweetText = [tweetText substringFromIndex:range.location+1];
                range = [tweetText rangeOfString:@" "];
                if (range.location != NSNotFound) {
                    tweetText = [tweetText substringToIndex:range.location];
                }
                newTweet.bankName = tweetText;
                needBankName = NO;
                
                tweetFullText = [self removeSubstring:[NSString stringWithFormat:@"@%@", newTweet.bankName] fromString:tweetFullText];
            }
        }
        
        NSArray *hashtags = [[tweet valueForKey:@"entities"] valueForKey:@"hashtags"];
        
        BOOL inlineFound = NO;
        BOOL mtowaitFound = NO;
        BOOL availFound = NO;
        BOOL closedFound = NO;
        BOOL dontwaitFound = NO;
        
        for (NSInteger hashtagIndex=mainTweetTagCount; hashtagIndex<hashtags.count; hashtagIndex++) {
            NSString *hashtag = [[hashtags objectAtIndex:hashtagIndex] valueForKey:@"text"];
            
            if (needBankName) {
                if (hashtagIndex == mainTweetBankNameIndex) {
                    newTweet.bankName = hashtag;
                    needBankName = NO;
                    
                    tweetFullText = [self removeSubstring:[NSString stringWithFormat:@"#%@", newTweet.bankName] fromString:tweetFullText];
                    continue;
                }
            }
            
            BOOL found = NO;
            NSRange range;
            if (!inlineFound) {
                range = [hashtag rangeOfString:@"inline"];
                if (range.location != NSNotFound) {
                    newTweet.aInline = [self numberFromString:[hashtag substringToIndex:range.location]];
                    found = newTweet.aInline != nil;
                    inlineFound = found;
                }
            }
            
            if (!found && !mtowaitFound) {
                range = [hashtag rangeOfString:@"mtowait"];
                if (range.location != NSNotFound) {
                    newTweet.mtowait = [self numberFromString:[hashtag substringToIndex:range.location]];
                    found = newTweet.mtowait != nil;
                    mtowaitFound = found;
                }
            }
            
            if (!found && !availFound) {
                range = [hashtag rangeOfString:@"avail"];
                if (range.location != NSNotFound) {
                    newTweet.avail = [self numberFromString:[hashtag substringToIndex:range.location]];
                    found = newTweet.avail != nil;
                    availFound = found;
                }
            }
            
            if (!found && !closedFound) {
                range = [hashtag rangeOfString:@"closed"];
                if (range.location != NSNotFound) {
                    newTweet.closed = [self numberFromString:[hashtag substringToIndex:range.location]];
                    found = newTweet.closed != nil;
                    closedFound = found;
                }
            }
            
            if (!found && !dontwaitFound) {
                range = [hashtag rangeOfString:@"dontwait"];
                if (range.location != NSNotFound) {
                    newTweet.dontwait = [NSNumber numberWithBool:YES];
                    found = YES;
                    dontwaitFound = YES;
                }
            }
            
            if (found) {
                tweetFullText = [self removeSubstring:[NSString stringWithFormat:@"#%@", hashtag] fromString:tweetFullText];
            }
        }
        
        newTweet.text = [tweetFullText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:usLocale];
        [dateFormatter setDateStyle:NSDateFormatterLongStyle];
        [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [dateFormatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
        NSDate *date = [dateFormatter dateFromString:[tweet valueForKey:@"created_at"]];
        newTweet.created = date;
        
        NSDictionary *user = [tweet valueForKey:@"user"];
        newTweet.userAccount = [user valueForKey:@"screen_name"];
        newTweet.userName = [user valueForKey:@"name"];
        newTweet.userImageUrl = [user valueForKey:@"profile_image_url"];
    }
    
    [appDelegate saveContext];
    
    return ids;
}

- (void)loadTweetsWithGeocode
{
    NSString *geocode;
#ifdef DEBUG
    geocode = @"55.755520,37.618217";
#else
    geocode = [NSString stringWithFormat:@"%f,%f", mainMapView.userLocation.location.coordinate.latitude, mainMapView.userLocation.location.coordinate.longitude];
#endif
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:mainTweetTag, @"q",
              [NSString stringWithFormat:@"%@,1000km", geocode], @"geocode",
              nil];
    [self loadTweetsWithParams:params];
}

- (void)loadTweetsWithBankAccount:(NSString *)bankAccount
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@%@", mainTweetTag, bankAccount], @"q",
                                   nil];
    [self loadTweetsWithParams:params];
}

- (void)loadTweetsWithParams:(NSMutableDictionary *)params
{
    [self loadTweetsWithParams:params reuseRequestWithIndex:-1];
}

- (void)loadTweetsWithParams:(NSMutableDictionary *)params reuseRequestWithIndex:(NSInteger)requestIndex
{
    needGeoTweets = NO;
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (!granted) {
            [self performSelectorOnMainThread:@selector(showTwitterAccessError) withObject:nil waitUntilDone:NO];
            return;
        }
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:udTwitterAccount];
        NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
        ACAccount *targetAccount;
        
        BOOL found = NO;
        for (int i=0; i < accounts.count && !found; i++) {
            targetAccount = [accounts objectAtIndex:i];
            found = ([targetAccount.username isEqualToString:userName]);
        }
        
        NSMutableString *url = [NSMutableString stringWithString:@"http://api.twitter.com/1.1/search/tweets.json"];
        if (nextTweetResults) {
            [url appendString:nextTweetResults];
            [params removeAllObjects];
        }
        else {
            [params setObject:@"recent" forKey:@"result_type"];
            [params setObject:[NSString stringWithFormat:@"%i", tweetsCount] forKey:@"count"];
        }
        NSURL *statusUpdateUrl = [NSURL URLWithString:url];
        
        TWRequest *request = [[TWRequest alloc] initWithURL:statusUpdateUrl parameters:params requestMethod:TWRequestMethodGET];
        [request setAccount:targetAccount];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ((requestIndex < 0) || (requestIndex >= requests.count)) {
                [requests addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:request, @"request", [NSNumber numberWithBool:NO], @"needCancel", nil]];
            }
            else {
                if (requestIndex < requests.count) {
                    [[requests objectAtIndex:requestIndex] setValue:request forKey:@"request"];
                }
            }
        });
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        });
        
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse*urlResponse, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    if (needTweetsListViewDone) {
                        needTweetsListViewDone = NO;
                        [self performSelector:@selector(tweetsListViewDataLoadingDone) withObject:self afterDelay:3.0];
                    }
                    return;
                }
                
                BOOL found = NO;
                for (NSInteger i=0; i < requests.count && !found; i++) {
                    if ([[requests objectAtIndex:i] valueForKey:@"request"] == request) {
                        found = YES;
                        if ([[[requests objectAtIndex:i] valueForKey:@"needCancel"] boolValue]) {
                            [requests removeObjectAtIndex:i];
                            return;
                        }
                    }
                }
                
                NSError *error;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Ошибка при получении списка твитов" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    if (needTweetsListViewDone) {
                        needTweetsListViewDone = NO;
                        [self performSelector:@selector(tweetsListViewDataLoadingDone) withObject:self afterDelay:3.0];
                    }
                    return;
                }
                
                NSArray *tweetsArray = [dict valueForKey:@"statuses"];
                NSArray *ids = [self addTweets:tweetsArray];
                [self addAnnotationsWithIDs:ids];
                
                NSDictionary *metadata = [dict valueForKey:@"search_metadata"];
                if ([metadata valueForKey:@"next_results"]) {
                    nextTweetResults = [metadata valueForKey:@"next_results"];
                    BOOL found = NO;
                    NSInteger reuseIndex = -1;
                    for (NSInteger i=0; i < requests.count && !found; i++) {
                        if ([[requests objectAtIndex:i] valueForKey:@"request"] == request) {
                            found = YES;
                            reuseIndex = i;
                        }
                    }
                    [self loadTweetsWithParams:params reuseRequestWithIndex:reuseIndex];
                }
                else {
                    nextTweetResults = nil;
                    BOOL found = NO;
                    for (NSInteger i=0; i < requests.count && !found; i++) {
                        if ([[requests objectAtIndex:i] valueForKey:@"request"] == request) {
                            found = YES;
                            [requests removeObjectAtIndex:i];
                        }
                    }
                }
                
                if (needTweetsListViewDone) {
                    needTweetsListViewDone = nextTweetResults != nil;
                    if (nextTweetResults) {
                        [self tweetsListViewDataLoadingDone];
                    }
                    else {
                        [self performSelector:@selector(tweetsListViewDataLoadingDone) withObject:self afterDelay:5.0];
                    }
                }
            });
        }];
    }];
}

- (void)tweetsListViewDataLoadingDone
{
    [tweetsListView dataLoadingDone];
}

- (void)removeAllTweets
{
    NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForIDs:nil];
    NSArray *tweets = fetchedResultsController.fetchedObjects;
    for (NSInteger i=0; i < tweets.count; i++) {
        Tweets *tweet = [tweets objectAtIndex:i];
        [appDelegate.managedObjectContext deleteObject:tweet];
    }
    [appDelegate saveContext];
    
    NSInteger i=0;
    while (i < mainMapView.annotations.count) {
        //if ([mainMapView.annotations objectAtIndex:i] != mainMapView.userLocation) {
        if ([[mainMapView.annotations objectAtIndex:i] isKindOfClass:[Annotation class]]) {
            [mainMapView removeAnnotation:[mainMapView.annotations objectAtIndex:i]];
        }
        else {
            i++;
        }
    }
}

- (void)addAllAnnotations
{
    [self addAnnotationsWithIDs:nil];
}

+ (LatLongRange)latLongRangeFromCoordinate:(CLLocationCoordinate2D)coordinate
{
    LatLongRange range;
    
    NSInteger locationLatInt = coordinate.latitude*latLongRound;
    NSInteger locationLongInt = coordinate.longitude*latLongRound;
    range.latMin = (double)locationLatInt/latLongRound - (double)5/latLongRound;
    range.latMax = range.latMin + (double)11/latLongRound;
    range.longMin = (double)locationLongInt/latLongRound - (double)5/latLongRound;
    range.longMax =  range.longMin + (double)11/latLongRound;
    
    return range;
}

- (NSString *)bankAccountForTweet:(NSDictionary *)tweet
{
    NSString *bankAccount;
    if ([tweet valueForKey:@"bankAccount"] && [[tweet valueForKey:@"bankAccount"] length] > 0) {
        bankAccount = [NSString stringWithFormat:@"@%@", [tweet valueForKey:@"bankAccount"]];
    }
    else {
        bankAccount = [NSString stringWithFormat:@"#%@", [tweet valueForKey:@"bankName"]];
        bankAccount = [bankAccount stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    }
    
    return bankAccount;
}

- (void)addAnnotationsWithIDs:(NSArray *)ids
{
    NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForIDs:ids];
    NSArray *tweets = fetchedResultsController.fetchedObjects;
    for (NSInteger i=0; i < tweets.count; i++) {
        Tweets *tweet = [tweets objectAtIndex:i];
        if (tweet.locationLat && tweet.locationLong) {
            LatLongRange range = [MapController latLongRangeFromCoordinate:CLLocationCoordinate2DMake(tweet.locationLat.floatValue, tweet.locationLong.floatValue)];
            BOOL found = NO;
            Annotation *foundAnnotation;
            for (NSInteger j=0; j < mainMapView.annotations.count && !found; j++) {
                //if ([mainMapView.annotations objectAtIndex:j] != mainMapView.userLocation) {
                if ([[mainMapView.annotations objectAtIndex:j] isKindOfClass:[Annotation class]]) {
                    Annotation *annotation = [mainMapView.annotations objectAtIndex:j];
                    found = (range.latMin < annotation.coordinate.latitude) && (annotation.coordinate.latitude < range.latMax) && (range.longMin < annotation.coordinate.longitude) && (annotation.coordinate.longitude < range.longMax);
                    if (found) {
                        foundAnnotation = annotation;
                    }
                }
            }
            
            // !! нельзя использовать dictionaryWithObjectsAndKeys, первый объект может быть nil
            NSMutableDictionary *tweetDict = [NSMutableDictionary dictionary];
            if (tweet.bankAccount) {
                [tweetDict setObject:tweet.bankAccount forKey:@"bankAccount"];
            }
            if (tweet.bankName) {
                [tweetDict setObject:tweet.bankName forKey:@"bankName"];
            }
            if (tweet.locationLat) {
                [tweetDict setObject:tweet.bankName forKey:@"locationLat"];
            }
            if (tweet.locationLong) {
                [tweetDict setObject:tweet.bankName forKey:@"locationLong"];
            }
            
            if (!found) {
                Annotation *annotation = [Annotation new];
                if (tweet.bankAccount) {
                    annotation.title = tweet.bankName;
                }
                else {
                    annotation.title = [NSString stringWithFormat:@"#%@", tweet.bankName];
                }
                annotation.subtitle = tweet.text;
                annotation.imageUrl = tweet.userImageUrl;
                annotation.coordinate = CLLocationCoordinate2DMake(tweet.locationLat.floatValue, tweet.locationLong.floatValue);
                annotation.tweet = [NSDictionary dictionaryWithObjectsAndKeys:[self bankAccountForTweet:tweetDict], @"bankAccount", tweet.bankName, @"bankName", tweet.locationLat, @"locationLat", tweet.locationLong, @"locationLong", nil];
                annotation.banks = [NSMutableArray arrayWithObject:annotation.tweet];
                [mainMapView addAnnotation:annotation];
            }
            else {
                BOOL foundBank = NO;
                for (NSInteger j=0; j < foundAnnotation.banks.count && !foundBank; j++) {
                    NSDictionary *bank = [foundAnnotation.banks objectAtIndex:j];
                    foundBank = ([[bank valueForKey:@"bankAccount"] isEqualToString:[self bankAccountForTweet:tweetDict]]);
                }
                
                if (!foundBank) {
                    [foundAnnotation.banks addObject:[NSDictionary dictionaryWithObjectsAndKeys:[self bankAccountForTweet:tweetDict], @"bankAccount", tweet.bankName, @"bankName", tweet.locationLat, @"locationLat", tweet.locationLong, @"locationLong", nil]];
                }
            }
        }
    }
}

- (void)showAnnotationBanks:(Annotation *)annotation
{
    AnnotationBanksView *annotationBanksView = [[[NSBundle mainBundle] loadNibNamed:@"AnnotationBanksView_iPhone" owner:self options:nil] lastObject];
    annotationBanksView.delegate = self;
    annotationBanksView.annotation = annotation;
    [annotationBanksView initView];
    [ASDepthModalViewController presentView:annotationBanksView withBackgroundColor:nil popupAnimationStyle:ASDepthModalAnimationShrink blur:YES];
    needTweetsListViewDone = YES;
}

- (void)cancelAllRequests
{
    for (NSInteger i=0; i < requests.count; i++) {
        [[requests objectAtIndex:i] setValue:[NSNumber numberWithBool:YES] forKey:@"needCancel"];
    }
}

- (void)showTweetsListWithTweet:(NSDictionary *)tweet
{
    nextTweetResults = nil;
    needGeoTweets = NO;
    [self cancelAllRequests];
    [self removeAllTweets];
    [self loadTweetsWithBankAccount:[self bankAccountForTweet:tweet]];
    
    tweetsListView = [[[NSBundle mainBundle] loadNibNamed:@"TweetsListView_iPhone" owner:self options:nil] lastObject];
    tweetsListView.delegate = self;
    tweetsListView.bankName = [tweet valueForKey:@"bankName"];
    tweetsListView.bankAccount = [tweet valueForKey:@"bankAccount"];
    if ([tweet valueForKey:@"locationLat"] && [tweet valueForKey:@"locationLong"]) {
        tweetsListView.showHeaderView = YES;
        CLLocationCoordinate2D bankCoordinate = CLLocationCoordinate2DMake([[tweet valueForKey:@"locationLat"] doubleValue], [[tweet valueForKey:@"locationLong"] doubleValue]);
        tweetsListView.bankCoordinate = bankCoordinate;
        tweetsListView.bankLatLongRange = [MapController latLongRangeFromCoordinate:bankCoordinate];
    }
    [tweetsListView initView];
    [ASDepthModalViewController presentView:tweetsListView withBackgroundColor:nil popupAnimationStyle:ASDepthModalAnimationShrink blur:YES];
    needTweetsListViewDone = YES;
}

#pragma mark - CoreData

- (NSFetchedResultsController *)fetchedResultsControllerForIDs:(NSArray *)ids
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Tweets" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if (ids && ids.count > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"webID IN %@", ids];
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:appDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
	NSError *error = nil;
	if (![aFetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return aFetchedResultsController;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (userLocation.location.horizontalAccuracy < 201) {
        if (!needAccount && needGeoTweets) {
            nextTweetResults = nil;
            [self removeAllTweets];
            [self loadTweetsWithGeocode];
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation) {
        return nil;
    }
    
    static NSString* customAnnotationIdentifier = @"customAnnotationIdentifier";
    MKAnnotationView* annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:customAnnotationIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:customAnnotationIdentifier];
        annotationView.image = [UIImage imageNamed:@"angryTweetMap"];
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.canShowCallout = YES;
        if (!((Annotation *)annotation).imageUrl) {
            annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:annotationView.image];;
        }
        else {
            UIImageView *sfIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32.f, 32.f)];
            sfIconView.contentMode = UIViewContentModeScaleAspectFill;
            [sfIconView setImageWithURL:[NSURL URLWithString:((Annotation *)annotation).imageUrl] placeholderImage:annotationView.image];
            annotationView.leftCalloutAccessoryView = sfIconView;
        }
        
        return annotationView;
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    Annotation *annotationTapped = (Annotation *)view.annotation;
    
    if (annotationTapped.banks.count < 2) {
        [self showTweetsListWithTweet:annotationTapped.tweet];
    }
    else {
        [self showAnnotationBanks:annotationTapped];
    }
}

#pragma mark - BankMasterContollerDelegate

- (void)bankMasterControllerDoneWithBankAccount:(NSString *)bankAccount bankName:(NSString *)bankName
{
    [ASDepthModalViewController dismiss];
    if (bankAccount) {
        NSMutableDictionary *tweetDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:bankAccount, @"bankAccount", bankName, @"bankName", nil];
        [self performSelector:@selector(showTweetsListWithTweet:) withObject:tweetDict afterDelay:0.5];
    }
}

#pragma mark - TweetsListViewDelegate

- (void)TweetsListViewDone
{
    [ASDepthModalViewController dismiss];
    [self cancelAllRequests];
}

- (void)TweetsListViewSetPosition:(CLLocationCoordinate2D)position
{
    [mainMapView setCenterCoordinate:position animated:YES];
}

#pragma mark - AnnotationBanksViewDelegate

- (void)AnnotationBanksViewDoneWithTweet:(NSDictionary *)tweet
{
    [self performSelector:@selector(showTweetsListWithTweet:) withObject:tweet afterDelay:1.0];
}

#pragma mark - EnableGeoTutorialViewDelegate

- (void)enableGeoTutorialViewDone
{
    [ASDepthModalViewController dismiss];
    geoViewVisible = NO;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self performSelector:@selector(showGeoErrorActionSheet) withObject:nil afterDelay:1.0];
    }
}

#pragma mark - AppDelegateProtocol

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [optionsActionSheet dismissWithClickedButtonIndex:optionsActionSheet.cancelButtonIndex animated:NO];
    [twitterActionSheet dismissWithClickedButtonIndex:twitterActionSheet.cancelButtonIndex animated:NO];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self hideSplash];
    
    if (needAccount) {
        [self performSelector:@selector(loginToTwitter) withObject:nil afterDelay:1.0];
    }
    else {
        [self performSelector:@selector(checkTwitterAccess) withObject:nil afterDelay:1.0];
    }
}

@end