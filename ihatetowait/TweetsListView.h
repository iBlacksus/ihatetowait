//
//  TweetsListController.h
//  ihatetowait
//
//  Created by iBlacksus on 01.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TweetsListCell.h"
#import "Types.h"

@protocol TweetsListViewDelegate;
@interface TweetsListView : UIView <UITableViewDataSource, UITableViewDelegate, TweetsListCelldelegate>
{
    IBOutlet UINavigationItem *mainNavigationItem;
    IBOutlet UITableView *mainTableView;
    IBOutlet UIView *loadingView;
    IBOutlet UIView *splashView;
    IBOutlet UIView *shadowView;
    IBOutlet UIImageView *hand;
    IBOutlet UIView *headerView;
    IBOutlet UIImageView *headerBankAvatar;
    IBOutlet UILabel *headerBankName;
    IBOutlet UILabel *headerBankAccount;
    IBOutlet UILabel *headerBankStreet;
    IBOutlet UILabel *bankBestTime;
    IBOutlet UILabel *bankBadTime;
    IBOutlet UIImageView *bankRating1;
    IBOutlet UIImageView *bankRating2;
    IBOutlet UIImageView *bankRating3;
    IBOutlet UIImageView *bankRating4;
    IBOutlet UIImageView *bankRating5;
    IBOutlet UILabel *bankRatingLabel;
}

@property (nonatomic) NSString *bankName;
@property (nonatomic) NSString *bankAccount;
@property (nonatomic) BOOL showHeaderView;
@property (nonatomic) CLLocationCoordinate2D bankCoordinate;
@property (nonatomic) LatLongRange bankLatLongRange;
@property (nonatomic) id <TweetsListViewDelegate> delegate;

- (IBAction)close:(id)sender;
- (void)dataLoadingDone;

- (void)initView;

@end

@protocol TweetsListViewDelegate

- (void)TweetsListViewDone;
- (void)TweetsListViewSetPosition:(CLLocationCoordinate2D)position;

@end