//
//  ViewController.m
//  ihatetowait
//
//  Created by iBlacksus on 19.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "SpacialMasterController.h"
#import "QueueController.h"
#import "BankController.h"
#import "QueueSizeController.h"
#import "TimeController.h"
#import "WindowsController.h"
#import "CommentController.h"
#import "TweetPreviewController.h"
#import "SendTweetController.h"
#import "CancelController.h"
#import "ASDepthModalViewController.h"
#import "InfoController.h"
#import "AddHashtagsController.h"

@interface SpacialMasterController ()
{
    QueueController *queue;
    CancelController *cancel;
    BankController *bank;
    BankController *bankDontWait;
    QueueSizeController *queueSize;
    TimeController *time;
    WindowsController *windows;
    CommentController *comment;
    TweetPreviewController *tweetPreview;
    TweetPreviewController *tweetPreviewDontWait;
    SendTweetController *sendTweet;
    SendTweetController *sendTweetDontWait;
    InfoController *info;
    AddHashtagsController *addHashtagsDontWait;
    AddHashtagsController *addHashtags;
    
    CLLocationManager *gps;
    CLLocation *location;
}

@end

@implementation SpacialMasterController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.hasMap = NO;
    self.view.backgroundColor = [UIColor clearColor];
    queue = [[QueueController alloc] initWithNibName:@"QueueView_iPhone" bundle:nil];
    self.root = queue;
    
    gps = [CLLocationManager new];
    gps.delegate = self;
    gps.desiredAccuracy = kCLLocationAccuracyBest;
    [gps startUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // не вызывается автоматически !!
    [queue spacialViewWillAppear];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // не вызывается автоматически !!
    [queue spacialViewDidAppear];
    
    // DOWN
    cancel = [[CancelController alloc] init];
    queue.lowerViewController = cancel;
    
    // LEFT
    bankDontWait = [[BankController alloc] init];
    bankDontWait.type = SendTweetTypeDontWait;
    queue.leftViewController = bankDontWait;
    
    tweetPreviewDontWait = [[TweetPreviewController alloc] init];
    tweetPreviewDontWait.mainViewController = self;
    tweetPreviewDontWait.type = SendTweetTypeDontWait;
    bankDontWait.leftViewController = tweetPreviewDontWait;
    
    sendTweetDontWait = [[SendTweetController alloc] init];
    sendTweetDontWait.mainViewController = self;
    tweetPreviewDontWait.upperViewController = sendTweetDontWait;
    
    addHashtagsDontWait = [[AddHashtagsController alloc] init];
    tweetPreviewDontWait.lowerViewController = addHashtagsDontWait;
    
    // RIGHT
    bank = [[BankController alloc] init];
    bank.type = SendTweetTypeNormal;
    queue.rightViewController = bank;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        info = [[InfoController alloc] init];
        info.mainViewController = self;
        bank.rightViewController = info;
        
        tweetPreview = [[TweetPreviewController alloc] init];
        tweetPreview.mainViewController = self;
        tweetPreview.type = SendTweetTypeNormal;
        info.rightViewController = tweetPreview;
    } else {
        queueSize = [[QueueSizeController alloc] init];
        queueSize.mainViewController = self;
        bank.rightViewController = queueSize;
        
        time = [[TimeController alloc] init];
        queueSize.rightViewController = time;
        
        windows = [[WindowsController alloc] init];
        time.rightViewController = windows;
        
        comment = [[CommentController alloc] init];
        comment.mainViewController = self;
        windows.rightViewController = comment;
        
        tweetPreview = [[TweetPreviewController alloc] init];
        tweetPreview.mainViewController = self;
        tweetPreview.type = SendTweetTypeNormal;
        comment.rightViewController = tweetPreview;
    }
    
    sendTweet = [[SendTweetController alloc] init];
    sendTweet.mainViewController = self;
    tweetPreview.upperViewController = sendTweet;
    
    addHashtags = [[AddHashtagsController alloc] init];
    tweetPreview.lowerViewController = addHashtags;
}

- (void)viewDidUnload
{
    gps = nil;
    location = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableString *)addText:(NSString *)text toTweet:(NSMutableString *)tweet
{
    if (text.length > 0) {
        [tweet appendFormat:@" %@", text];
    }
    
    return tweet;
}

- (NSString *)tweetTextForType:(SendTweetType)type
{
    NSMutableString *tweet = [NSMutableString stringWithString:mainTweetTag];
    
    switch (type) {
        case SendTweetTypeDontWait:
            tweet = [self addText:[bankDontWait tweetText] toTweet:tweet];
            tweet = [self addText:@"#dontwait не дождался очереди :-/" toTweet:tweet];
            tweet = [self addText:[addHashtagsDontWait tweetTextForLength:tweet.length] toTweet:tweet];
            return tweet;
            
        case SendTweetTypeNormal:
            tweet = [self addText:[bank tweetText] toTweet:tweet];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                tweet = [self addText:[info tweetText] toTweet:tweet];
            }
            else {
                tweet = [self addText:[queueSize tweetText] toTweet:tweet];
                tweet = [self addText:[time tweetText] toTweet:tweet];
                tweet = [self addText:[windows tweetText] toTweet:tweet];
                tweet = [self addText:[comment tweetText] toTweet:tweet];
            }
            tweet = [self addText:[addHashtags tweetTextForLength:tweet.length] toTweet:tweet];
            
            return tweet;
    }
}

- (NSInteger)tweetLengthForComment
{
    NSMutableString *tweet = [NSMutableString stringWithString:mainTweetTag];
    tweet = [self addText:[bank tweetText] toTweet:tweet];
    tweet = [self addText:[queueSize tweetText] toTweet:tweet];
    tweet = [self addText:[time tweetText] toTweet:tweet];
    tweet = [self addText:[windows tweetText] toTweet:tweet];

    return tweet.length;
}

- (NSString *)bankForType:(SendTweetType)type;
{
    switch (type) {
        case SendTweetTypeDontWait:
            return [bankDontWait tweetText];
        case SendTweetTypeNormal:
            return [bank tweetText];
    }
}

- (NSString *)bankNameForType:(SendTweetType)type;
{
    switch (type) {
        case SendTweetTypeDontWait:
            return [bankDontWait bankName];
        case SendTweetTypeNormal:
            return [bank bankName];
    }
}

- (void)updateLocation
{
    [gps stopUpdatingLocation];
    [gps startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)locationManager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;
{
    location = newLocation;
    sendTweet.location = location;
    [sendTweet locationUpdated];
    sendTweetDontWait.location = location;
    [sendTweetDontWait locationUpdated];
    
    if (location.horizontalAccuracy < 100) {
        [gps stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Приложение не может получить доступ к Вашей геопозиции. Проверьте настройки геопозиции в настройках Вашего устройства, а так же разрешите доступ приложению." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location = [locations lastObject];
    sendTweet.location = location;
    [sendTweet locationUpdated];
    sendTweetDontWait.location = location;
    [sendTweetDontWait locationUpdated];
    
    if (location.horizontalAccuracy < 100) {
        [gps stopUpdatingLocation];
    }
}

#pragma mark - Static

+ (CGRect)childFrame
{
    return CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-20.f);
}

+ (NSString *)nibSuffix
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return @"iPhone";
    } else {
        return @"iPad";
    }
}

@end
