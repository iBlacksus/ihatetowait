//
//  InfoController.m
//  ihatetowait
//
//  Created by iBlacksus on 23.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "InfoController.h"
#import "SpacialMasterController.h"
#import "InfoView.h"
#import "BankController.h"

@interface InfoController ()
{
    InfoView *mainView;
}

@end

@implementation InfoController

@synthesize mainViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:@"InfoView_iPad" owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    mainView.commentView.delegate = self;
    mainView.queueSizeView.delegate = self;
    mainView.timeView.delegate = self;
    mainView.windowsView.delegate = self;
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    mainView.bankLabel.text = [mainViewController bankNameForType:SendTweetTypeNormal];
    
    [mainView.commentView.commentTextView becomeFirstResponder];
    [self updateCommentMaxLength];
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    if ([mainViewController bankForType:SendTweetTypeNormal].length == 0) {
        ((BankController *)self.leftViewController).needBankAlert = YES;
        [self moveInDirection:MBDirectionLeft animated:YES];
    }
    
    // Обход бага UITextView на iPad
    mainView.commentView.commentTextView.frame = mainView.commentView.commentTextView.frame;
}

- (void)spacialViewWillDisappear
{
    [super spacialViewWillDisappear];
    
    [mainView.commentView.commentTextView resignFirstResponder];
}

- (void)updateCommentMaxLength
{
    mainView.commentView.commentMaxLength = tweetMaxLength - [self tweetLengthForComment];
    [mainView.commentView updateCountLabelForText:mainView.commentView.commentTextView.text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableString *)addText:(NSString *)text toTweet:(NSMutableString *)tweet
{
    if (text.length > 0) {
        [tweet appendFormat:@" %@", text];
    }
    
    return tweet;
}

- (NSString *)queueSizeTweetText
{
    NSInteger size = [mainView.queueSizeView queueSize];
    if (size > 0) {
        return [NSString stringWithFormat:@"#%iinline", size];
    }
    
    return @"";
}

- (NSString *)timeTweetText
{
    NSInteger minutes = [mainView.timeView minutes];
    if (minutes > 0) {
        return [NSString stringWithFormat:@"#%imtowait", minutes];
    }
    
    return @"";
}

- (NSString *)windowsTweetText
{
    NSInteger openedWindows = [mainView.windowsView openedWindows];
    NSInteger closedWindows = [mainView.windowsView closedWindows];
    NSMutableString *windows = [NSMutableString string];
    if (openedWindows > 0) {
        [windows appendFormat:@"#%iavail", openedWindows];
    }
    if (closedWindows > 0) {
        if (windows.length > 0) {
            [windows appendString:@" "];
        }
        [windows appendFormat:@"#%iclosed", closedWindows];
    }
    
    return windows;
}

- (NSString *)commentTweetText
{
    return [mainView.commentView comment];
}

- (NSString *)tweetText
{
    NSMutableString *tweet = [NSMutableString string];
    
    tweet = [self addText:[self queueSizeTweetText] toTweet:tweet];
    tweet = [self addText:[self timeTweetText] toTweet:tweet];
    tweet = [self addText:[self windowsTweetText] toTweet:tweet];
    tweet = [self addText:[self commentTweetText] toTweet:tweet];
    
    return tweet;
}

- (NSInteger)tweetLengthForComment
{
    NSMutableString *tweet = [NSMutableString stringWithString:mainTweetTag];
    tweet = [self addText:[(SpacialMasterController *)mainViewController bankForType:SendTweetTypeNormal] toTweet:tweet];
    tweet = [self addText:[self queueSizeTweetText] toTweet:tweet];
    tweet = [self addText:[self timeTweetText] toTweet:tweet];
    tweet = [self addText:[self windowsTweetText] toTweet:tweet];
    
    return tweet.length;
}

#pragma mark - CommentViewDelegate

- (void)commentViewDone
{
    [self moveInDirection:MBDirectionRight animated:YES];
}

#pragma mark - QueueSizeViewDelegate

- (void)queueSizeViewValueChanged
{
    [self updateCommentMaxLength];
}

#pragma mark - TimeViewDelegate

- (void)timeViewValueChanged
{
    [self updateCommentMaxLength];
}

#pragma mark - WindowsViewDelegate

- (void)windowsViewValueChanged
{
    [self updateCommentMaxLength];
}

@end
