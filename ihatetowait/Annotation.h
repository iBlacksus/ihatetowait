//
//  Annotation.h
//  ihatetowait
//
//  Created by iBlacksus on 09.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Tweets.h"

@interface Annotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subtitle;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSMutableArray *banks;
@property (nonatomic) NSDictionary *tweet;

@end
