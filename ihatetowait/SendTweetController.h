//
//  SendTweetController.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "MBSpacialChildViewController.h"
#import <CoreLocation/CLLocation.h>
#import "Types.h"

@interface SendTweetController : MBSpacialChildViewController <UIAlertViewDelegate>

@property (nonatomic) CLLocation *location;
@property (nonatomic) SendTweetType type;
@property (nonatomic) NSString *tweetText;
@property (nonatomic) id mainViewController;

@property (nonatomic) NSString *locationLat;
@property (nonatomic) NSString *locationLong;

- (void)spacialViewDidLoad;
- (void)spacialViewWillAppear;
- (void)spacialViewDidAppear;
- (void)spacialViewDidDisappear;

- (void)locationUpdated;

@end
