//
//  Banks.h
//  ihatetowait
//
//  Created by iBlacksus on 22.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Banks : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * account;

@end
