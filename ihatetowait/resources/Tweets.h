//
//  Tweets.h
//  ihatetowait
//
//  Created by iBlacksus on 08.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tweets : NSManagedObject

@property (nonatomic, retain) NSNumber * aInline;
@property (nonatomic, retain) NSNumber * avail;
@property (nonatomic, retain) NSString * bankAccount;
@property (nonatomic, retain) NSString * bankName;
@property (nonatomic, retain) NSNumber * closed;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * locationLat;
@property (nonatomic, retain) NSString * locationLong;
@property (nonatomic, retain) NSNumber * mtowait;
//@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * userAccount;
@property (nonatomic, retain) NSString * userImageUrl;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSNumber * webID;
@property (nonatomic, retain) NSNumber * dontwait;

@end
