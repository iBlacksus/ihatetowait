//
//  Tweets.m
//  ihatetowait
//
//  Created by iBlacksus on 08.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "Tweets.h"


@implementation Tweets

@dynamic aInline;
@dynamic avail;
@dynamic bankAccount;
@dynamic bankName;
@dynamic closed;
@dynamic created;
@dynamic locationLat;
@dynamic locationLong;
@dynamic mtowait;
//@dynamic tags;
@dynamic text;
@dynamic userAccount;
@dynamic userImageUrl;
@dynamic userName;
@dynamic webID;
@dynamic dontwait;

@end
