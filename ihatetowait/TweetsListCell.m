//
//  TweetsListCell.m
//  ihatetowait
//
//  Created by iBlacksus on 01.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TweetsListCell.h"

@implementation TweetsListCell

@synthesize delegate;
@synthesize tweetPosition;
@synthesize havePosition;
@synthesize mainView;
@synthesize statView;
@synthesize swipeType;
@synthesize swipeGesture;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)locateTweetPosition:(id)sender
{
    if (havePosition) {
        [delegate TweetsListCell:self locateTweetWithPosition:tweetPosition];
    }
}

- (void)swipe
{
    CGRect mainFrame = mainView.frame;
    mainFrame.origin.x = (swipeType == TweetsListCellSwipeTypeLeft) ? 0.f-mainView.frame.size.width : 0.f;
    CGRect statFrame = statView.frame;
    statFrame.origin.x = (swipeType == TweetsListCellSwipeTypeLeft) ? 0.f : statView.frame.size.width;
    
    swipeGesture.direction = (swipeType == TweetsListCellSwipeTypeLeft) ? UISwipeGestureRecognizerDirectionRight : UISwipeGestureRecognizerDirectionLeft;
    
    // в самом конце!!
    swipeType = (swipeType == TweetsListCellSwipeTypeLeft) ? TweetsListCellSwipeTypeRight : TweetsListCellSwipeTypeLeft;
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         mainView.frame = mainFrame;
                         statView.frame = statFrame;
                     }
                     completion:nil
     ];
}

@end
