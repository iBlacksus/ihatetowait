//
//  QueueView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueueController.h"

@interface QueueView : UIView
{
    IBOutlet UITextField *textField;    
    IBOutlet UIView *cancelView;
    IBOutlet UIImageView *leftHand;
    IBOutlet UIImageView *rightHand;
    IBOutlet UIView *noView;
    IBOutlet UIView *yesView;
    IBOutlet UIView *splashView;
    IBOutlet UIView *shadowView;
}

@property (nonatomic) QueueController *queueController;

- (IBAction)play:(id)sender;
- (IBAction)help:(id)sender;

- (void)initView;
- (void)viewWillAppear;
- (void)viewDidAppear;

@end