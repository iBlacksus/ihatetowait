//
//  SendTweetController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TweetPreviewController.h"
#import "TweetPreviewView.h"
#import "SpacialMasterController.h"
#import "SendTweetController.h"
#import "BankController.h"

@interface TweetPreviewController ()
{
    TweetPreviewView *mainView;
    UIAlertView *chooseBankAlertView;
}

@end

@implementation TweetPreviewController

@synthesize type;
@synthesize mainViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"TweetPreviewView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    mainView.sendTweetController = (SendTweetController *)self.upperViewController;
    
    mainView.topLeftArrow.hidden = type == SendTweetTypeDontWait;
    mainView.topRightArrow.hidden = type == SendTweetTypeNormal;
    
    mainView.tweetTextView.text = [(SpacialMasterController *)mainViewController tweetTextForType:type];
    ((SendTweetController *)self.upperViewController).tweetText = mainView.tweetTextView.text;
}

- (void)spacialViewDidAppear
{
    [super spacialViewDidAppear];
    
    if (type == SendTweetTypeDontWait) {
        if ([mainViewController bankForType:SendTweetTypeDontWait].length == 0) {
            ((BankController *)self.rightViewController).needBankAlert = YES;
            [self moveInDirection:MBDirectionRight animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
