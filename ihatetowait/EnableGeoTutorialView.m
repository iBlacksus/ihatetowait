//
//  EGTStep1Controller.m
//  ihatetowait
//
//  Created by iBlacksus on 11.07.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "EnableGeoTutorialView.h"
#import "ASDepthModalViewController.h"
#import "SpacialMasterController.h"

@interface EnableGeoTutorialView ()

@end

@implementation EnableGeoTutorialView

@synthesize delegate;

- (void)initView
{
    self.frame = [SpacialMasterController childFrame];
}

- (IBAction)done:(id)sender
{
    [delegate enableGeoTutorialViewDone];
}

- (IBAction)copyLink:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = twitterAccountUrlLabel.text;
}

- (IBAction)open:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterAccountUrlLabel.text]];
}

@end
