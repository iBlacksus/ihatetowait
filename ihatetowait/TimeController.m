//
//  TimeController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "TimeController.h"
#import "SpacialMasterController.h"
#import "TimeView.h"

@interface TimeController ()
{
    TimeView *mainView;
}

@end

@implementation TimeController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"TimeView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    [self.view addSubview:mainView];
    
    if ([SpacialMasterController childFrame].size.height < 500.f) {
        CGRect frame = mainView.clockImageView.frame;
        frame.origin.y -= 20.f;
        mainView.clockImageView.frame = frame;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tweetText
{
    NSInteger minutes = [mainView minutes];
    
    if (minutes > 0) {
        return [NSString stringWithFormat:@"#%imtowait", minutes];
    }
    
    return @"";
}

@end
