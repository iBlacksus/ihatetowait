//
//  CommentController.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "CommentController.h"
#import "CommentView.h"
#import "SpacialMasterController.h"
#import "Types.h"

@interface CommentController ()
{
    CommentView *mainView;
}

@end

@implementation CommentController

@synthesize mainViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)spacialViewDidLoad
{
    [super spacialViewDidLoad];
	// Do any additional setup after loading the view.
    mainView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"CommentView_%@", [SpacialMasterController nibSuffix]] owner:self options:nil] lastObject];
    mainView.frame = [SpacialMasterController childFrame];
    mainView.delegate = self;
    [self.view addSubview:mainView];
}

- (void)spacialViewWillAppear
{
    [super spacialViewWillAppear];
    
    [mainView.commentTextView becomeFirstResponder];
    mainView.commentMaxLength = tweetMaxLength - [(SpacialMasterController *)mainViewController tweetLengthForComment];
    [mainView updateCountLabelForText:mainView.commentTextView.text];
}

- (void)spacialViewWillDisappear
{
    [super spacialViewWillDisappear];
    
    [mainView.commentTextView resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tweetText
{
    return [mainView comment];
}

#pragma mark - CommentViewDelegate

- (void)commentViewDone
{
    [self moveInDirection:MBDirectionRight animated:YES];
}

@end
