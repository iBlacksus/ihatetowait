//
//  TimeView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TimeViewDelegate;
@interface TimeView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIPickerView *timePicker;
}

@property (nonatomic, strong) IBOutlet UIImageView *clockImageView;
@property (nonatomic) id <TimeViewDelegate> delegate;

- (NSInteger)minutes;
+ (NSString *)timeFromMinutes:(NSInteger)minutes;
+ (NSString *)caseForWordByNumber:(NSInteger)number variants:(NSArray *)variants;

@end

@protocol TimeViewDelegate

- (void)timeViewValueChanged;

@end