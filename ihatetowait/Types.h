//
//  Types.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#ifndef ihatetowait_Types_h
#define ihatetowait_Types_h

#define tweetMaxLength 140
#define udTwitterAccount @"username"
#define udDontCheckGeoEnabled @"dontCheckGeoEnabled"

#ifdef DEBUG
    #define mainTweetTag @"#ihatetowaitapp"
    #define mainTweetTagCount 1
#else
    #define mainTweetTag @"#ihatetowait #app"
    #define mainTweetTagCount 2
#endif

#define mainTweetBankNameIndex mainTweetTagCount

typedef enum {
    SendTweetTypeDontWait = 0,
    SendTweetTypeNormal
} SendTweetType;

typedef enum {
    BankControllerTypeNormal = 0,
    BankControllerTypeModal
} BankControllerType;

struct LatLongRange {
    double latMin;
    double longMin;
    double latMax;
    double longMax;
};
typedef struct LatLongRange LatLongRange;

#endif
