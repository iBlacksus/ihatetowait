//
//  BankView.m
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import "BankView.h"
#import "AppDelegate.h"
#import "Banks.h"
#import <AudioToolbox/AudioToolbox.h>

#define udBanksLoaded @"banksLoaded"
#define splashLabelBeginType @"Начните вводить название"
#define splashLabelNewBank @"Нет нужного банка?"

@implementation BankView
{
    NSFetchedResultsController *banksFetchedResultsController;
    NSString *lastString;
    AppDelegate *appDelegate;
    BOOL splashCanScroll;
    CGFloat splashY;
    UIBarButtonItem *cancelButton;
}

@synthesize mainTableView;
@synthesize mainSearchBar;
@synthesize bankAccount;
@synthesize bankName;
@synthesize delegate;
@synthesize bankTextField;
@synthesize type;
@synthesize controllerType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initView
{
    lastString = [NSString string];
    splashCanScroll = NO;
    
    splashY = 44.f;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:udBanksLoaded]) {
        [self loadBanks];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udBanksLoaded];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    banksFetchedResultsController = [self fetchedResultsControllerForString:@""];
    
    bankAccount = [NSString string];
    bankName = [NSString string];
    [self setArrowsVisiblity:NO];
}

- (void)loadBanks
{
    NSData *data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/banks.json", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось загрузить список банков" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    for (NSInteger i=0; i < array.count; i++) {
        NSDictionary *dict = [array objectAtIndex:i];
        
        Banks *newBank = [NSEntityDescription insertNewObjectForEntityForName:@"Banks" inManagedObjectContext:appDelegate.managedObjectContext];
        newBank.name = [dict valueForKey:@"name"];
        newBank.account = [dict valueForKey:@"account"];
    }
    
    [appDelegate saveContext];
    
    [mainTableView reloadData];
}

- (IBAction)cancelNewBank:(id)sender
{
    splashY = 44.f;
    mainTableView.contentOffset = CGPointMake(0, -44.f);
    splashCanScroll = YES;
    [mainTableView setContentOffset:CGPointZero animated:YES];
    [mainSearchBar becomeFirstResponder];
    disableSearchView.hidden = YES;
    splashView.userInteractionEnabled = NO;
    bankTextField.text = @"";
    [self showSplash];
    [mainTableView reloadData];
}

- (void)cancel
{
    [delegate bankViewCancel];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    splashCanScroll = NO;
    newBankView.hidden = YES;
    
    if (mainSearchBar.text.length == 0) {
        splashLabel.text = splashLabelBeginType;
        splashView.hidden = NO;
    }
    else if ((banksFetchedResultsController.fetchedObjects.count == 0) && (controllerType == BankControllerTypeNormal)) {
        splashLabel.text = splashLabelNewBank;
        splashView.hidden = NO;
        splashCanScroll = YES;
        newBankView.hidden = NO;
    }
    else {
        splashView.hidden = YES;
    }
    
    return banksFetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BankCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Banks *bank = [banksFetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = bank.name;
    cell.detailTextLabel.text = bank.account;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Banks *bank = [banksFetchedResultsController objectAtIndexPath:indexPath];
    if (bank.account.length > 0) {
        bankAccount = bank.account;
    }
    else {
        bankAccount = [NSString stringWithFormat:@"#%@", bank.name];
        bankAccount = [bankAccount stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    }
    bankName = bank.name;
    
    [self setArrowsVisiblity:YES];
    
    [delegate bankViewDone];
}

#pragma mark - CoreData

- (NSFetchedResultsController *)fetchedResultsControllerForString:(NSString *)string
{
    if (![string isEqualToString:lastString]) {
        lastString = string;
        banksFetchedResultsController = nil;
    }
    
    if (banksFetchedResultsController)
    {
        return banksFetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Banks" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    string = [string stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
    
    if (string && string.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name CONTAINS[cd] '%@'", string]];
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:appDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
	NSError *error = nil;
	if (![aFetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    banksFetchedResultsController = aFetchedResultsController;
    return banksFetchedResultsController;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    banksFetchedResultsController = [self fetchedResultsControllerForString:searchText];
    [mainTableView reloadData];
    bankAccount = @"";
    bankName = @"";
    [self setArrowsVisiblity:NO];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!splashCanScroll) {
        return;
    }
    
    CGRect frame = splashView.frame;
    CGFloat y = mainTableView.contentOffset.y * -1 + 44.f;
    if (y < splashY) {
        y = splashY;
        mainTableView.contentOffset = CGPointZero;
    }
    frame.origin.y = y;
    splashView.frame = frame;
}

- (void)setArrowsVisiblity:(BOOL)visible
{
    if (controllerType == BankControllerTypeModal) {
        rightArrow.hidden = YES;
        leftArrow.hidden = YES;
        if (!cancelButton) {
            cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Отмена" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
            bankNavigationItem.rightBarButtonItem = cancelButton;
        }
        
        return;
    }
    
    if (type == SendTweetTypeDontWait) {
        rightArrow.alpha = 1.f;
    }
    else if (type == SendTweetTypeNormal) {
        leftArrow.alpha = 1.f;
    }
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if (type == SendTweetTypeDontWait) {
                             leftArrow.alpha = (visible) ? 1.f : 0.f;
                         }
                         else if (type == SendTweetTypeNormal) {
                             rightArrow.alpha = (visible) ? 1.f : 0.f;
                         }
                     }
                     completion:nil
     ];
}

- (void)hideSplash
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         splashLabel.alpha = 0.f;
                         newBankView.alpha = 0.f;
                     }
                     completion:^(BOOL finished) {
                         splashLabel.hidden = YES;
                         newBankView.hidden = YES;
                     }
     ];
}

- (void)showSplash
{
    splashLabel.hidden = NO;
    newBankView.hidden = NO;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         splashLabel.alpha = 0.5;
                         newBankView.alpha = 0.5;
                     }
                     completion:nil
     ];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!splashCanScroll) {
        return;
    }
    
    if (mainTableView.contentOffset.y < -44.f) {
        splashY = 88.f;
        [bankTextField becomeFirstResponder];
        disableSearchView.hidden = NO;
        splashView.userInteractionEnabled = YES;
        bankTextField.text = mainSearchBar.text;
        [self hideSplash];
        AudioServicesPlaySystemSound(1103);
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!splashCanScroll) {
        return;
    }
    
    if (splashY == 88.f) {
        splashCanScroll = NO;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == bankTextField) {
        NSString *acc = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        acc = [acc stringByReplacingOccurrencesOfString:@"#" withString:@""];
        acc = [acc stringByReplacingOccurrencesOfString:@"'" withString:@""];
        acc = [acc stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        Banks *newBank = [NSEntityDescription insertNewObjectForEntityForName:@"Banks" inManagedObjectContext:appDelegate.managedObjectContext];
        newBank.name = textField.text;
        newBank.account = [NSString stringWithFormat:@"#%@", acc];
        [appDelegate saveContext];
        mainSearchBar.text = textField.text;
        [self cancelNewBank:nil];
        lastString = @"";
        [self searchBar:mainSearchBar textDidChange:mainSearchBar.text];
        [mainTableView setContentOffset:CGPointZero animated:NO];
        CGRect frame = splashView.frame;
        frame.origin.y = 44.f;
        splashView.frame = frame;
    }
    
    return YES;
}

#pragma mark - Keyboard Notifications

- (void)onKeyboardShow:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    mainTableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrameBeginRect.size.height, 0);
}

- (void)onKeyboardHide:(NSNotification*)notification
{
    mainTableView.contentInset = UIEdgeInsetsZero;
}

@end