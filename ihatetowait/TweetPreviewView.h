//
//  SendTweetView.h
//  ihatetowait
//
//  Created by iBlacksus on 20.06.13.
//  Copyright (c) 2013 iBlacksus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendTweetController.h"

@interface TweetPreviewView : UIView
{
    IBOutlet UIImageView *arrowImageView;
    IBOutlet UITextField *locationLat;
    IBOutlet UITextField *locationLong;
    
}

@property (nonatomic) SendTweetController *sendTweetController;

@property (strong, nonatomic) IBOutlet UITextView *tweetTextView;
@property (strong, nonatomic) IBOutlet UIImageView *topLeftArrow;
@property (strong, nonatomic) IBOutlet UIImageView *topRightArrow;

- (IBAction)setLatLong:(id)sender;

@end
